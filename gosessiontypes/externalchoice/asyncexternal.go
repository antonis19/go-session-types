package main

import (
	"fmt"
)

type InputChannel struct {
	intChannel    <-chan int
	stringChannel <-chan string
}

type OutputChannel struct {
	intChannel    chan<- int
	stringChannel chan<- string
}

type Channel struct {
	intChannel    chan int
	stringChannel chan string
}

type SessionChannel struct {
	in  *InputChannel
	out *OutputChannel
}

func (channel *Channel) make(bufsize int) {
	channel.intChannel = make(chan int, bufsize)
	channel.stringChannel = make(chan string, bufsize)
}

// Because Go does not allow casting of Channel to InChannel
// we define this function to make an InChannel copy of
// the given Channel
func (in *InputChannel) make(ch *Channel) {
	in.intChannel = ch.intChannel
	in.stringChannel = ch.stringChannel
}

// Because Go does not allow casting of Channel to OutChannel
// we define this function to make an OutChannel copy of
// the given Channel
func (out *OutputChannel) make(ch *Channel) {
	out.intChannel = ch.intChannel
	out.stringChannel = ch.stringChannel
}

func (schan *SessionChannel) make() {
	schan.in = new(InputChannel)
	schan.out = new(OutputChannel)
}

func makeSessionChannelPair(bufsize int) (schan1 *SessionChannel, schan2 *SessionChannel) {
	var in, out = new(Channel), new(Channel)
	schan1, schan2 = new(SessionChannel), new(SessionChannel)
	schan1.make()
	schan2.make()
	in.make(bufsize)
	out.make(bufsize)
	schan1.in.make(in)
	schan1.out.make(out)
	schan2.in.make(out) // schan1.out == schan1.in
	schan2.out.make(in) // schan1.in == schan2.out
	return
}

var x = 31

func internalChoice(schan *SessionChannel, endChannel chan bool) {
	if x < 42 {
		// !int
		schan.out.intChannel <- 99
		endChannel <- true
	} else {
		// ?string
		<-schan.in.stringChannel
		endChannel <- true
	}
}

func externalChoice(schan *SessionChannel, endChannel chan bool) {
	select {
	// ?int
	case <-schan.in.intChannel:
		fmt.Println("received an int")
		endChannel <- true
	// !string
	case schan.out.stringChannel <- "Hello World":
		fmt.Println("sent a string")
		endChannel <- true
	}
}

func main() {
	bufsize := 4
	schan_1, schan_2 := makeSessionChannelPair(bufsize)
	endChannel := make(chan bool)
	go externalChoice(schan_1, endChannel)
	go internalChoice(schan_2, endChannel)
	<-endChannel
	<-endChannel
}
