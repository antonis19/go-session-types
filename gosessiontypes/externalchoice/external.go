package main

import (
	"fmt"
)

type SessionChannel struct {
	intChannel    chan int
	boolChannel   chan bool
	stringChannel chan string
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel{
		intChannel:    make(chan int),
		boolChannel:   make(chan bool),
		stringChannel: make(chan string),
	}
}

func (schan *SessionChannel) make() {
	schan.intChannel = make(chan int)
	schan.boolChannel = make(chan bool)
	schan.stringChannel = make(chan string)
}

var x = 31

func internalChoice(schan *SessionChannel) {
	if x < 42 {
		// !int
		schan.intChannel <- 99
	} else {
		// ?string
		<-schan.stringChannel
	}
}

func externalChoice(schan *SessionChannel) {
	select {
	case <-schan.intChannel:
		fmt.Println("received an int")
	case schan.stringChannel <- "Hello World":
		fmt.Println("sent a string")
	}
}

func main() {
	schan := new(SessionChannel)
	schan.make()
	go externalChoice(schan)
	internalChoice(schan)
}
