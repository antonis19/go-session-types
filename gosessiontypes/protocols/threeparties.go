package main

import (
	"fmt"
)

func partyA(ch chan int, endChannel chan bool) {
	ch <- 42
	endChannel <- true
}

func partyB(ch chan string, endChannel chan bool) {
	ch <- "Hello World"
	endChannel <- true
}

func partyC(intChannel chan int, stringChannel chan string, endChannel chan bool) {
	select {
	case <-intChannel:
		fmt.Println("Received int")
		endChannel <- true
	case <-stringChannel:
		fmt.Println("Received string")
		endChannel <- true
	}
}

func main() {
	endChannel := make(chan bool)
	intChannel, stringChannel := make(chan int), make(chan string)
	go partyA(intChannel, endChannel)
	go partyB(stringChannel, endChannel)
	go partyC(intChannel, stringChannel, endChannel)
	<-endChannel
	<-endChannel
	<-endChannel
}
