package main

type Address string
type Date string
type Bye struct{}

type InputChannel struct {
	intChannel     <-chan int
	stringChannel  <-chan string
	addressChannel <-chan Address
	dateChannel    <-chan Date
	byeChannel     <-chan Bye
}

type OutputChannel struct {
	intChannel     chan<- int
	stringChannel  chan<- string
	addressChannel chan<- Address
	dateChannel    chan<- Date
	byeChannel     chan<- Bye
}

type Channel struct {
	intChannel     chan int
	stringChannel  chan string
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

type SessionChannel struct {
	in  *InputChannel
	out *OutputChannel
}

func (channel *Channel) make(bufsize int) {
	channel.intChannel = make(chan int, bufsize)
	channel.stringChannel = make(chan string, bufsize)
	channel.addressChannel = make(chan Address, bufsize)
	channel.dateChannel = make(chan Date, bufsize)
	channel.byeChannel = make(chan Bye, bufsize)
}

// Because Go does not allow casting of Channel to InChannel
// we define this function to make an InChannel copy of
// the given Channel
func (in *InputChannel) make(ch *Channel) {
	in.intChannel = ch.intChannel
	in.stringChannel = ch.stringChannel
	in.addressChannel = ch.addressChannel
	in.dateChannel = ch.dateChannel
	in.byeChannel = ch.byeChannel
}

// Because Go does not allow casting of Channel to OutChannel
// we define this function to make an OutChannel copy of
// the given Channel
func (out *OutputChannel) make(ch *Channel) {
	out.intChannel = ch.intChannel
	out.stringChannel = ch.stringChannel
	out.addressChannel = ch.addressChannel
	out.dateChannel = ch.dateChannel
	out.byeChannel = ch.byeChannel
}

func (schan *SessionChannel) make() {
	schan.in = new(InputChannel)
	schan.out = new(OutputChannel)
}

func makeSessionChannelPair(bufsize int) (schan1 *SessionChannel, schan2 *SessionChannel) {
	var in, out = new(Channel), new(Channel)
	schan1, schan2 = new(SessionChannel), new(SessionChannel)
	schan1.make()
	schan2.make()
	in.make(bufsize)
	out.make(bufsize)
	schan1.in.make(in)
	schan1.out.make(out)
	schan2.in.make(out) // schan1.out == schan1.in
	schan2.out.make(in) // schan1.in == schan2.out
	return
}

func buyer1(sChanBuyer2 *SessionChannel, sChanSeller *SessionChannel, endChannel chan<- bool) {
	title := "The Go Programming Language"
	// Buyer1 -> Seller string
	sChanSeller.out.stringChannel <- title
	// Seller -> Buyer1 int
	quote := <-sChanSeller.in.intChannel
	println("[Buyer1] quote is ", quote)
	// Buyer1 -> Buyer2 string
	sChanBuyer2.out.intChannel <- quote / 2
	// endChannel
	endChannel <- true
}

func buyer2(sChanBuyer1 *SessionChannel, sChanSeller *SessionChannel, endChannel chan<- bool) {
	funds := 30
	// Seller -> Buyer2 int
	quote := <-sChanSeller.in.intChannel
	println("[Buyer2] quote is ", quote)
	// Buyer1 -> Buyer2 int
	buyer1Quote := <-sChanBuyer1.in.intChannel
	println("[Buyer2] buyer1Quote is", buyer1Quote)
	// internal choice
	if quote-buyer1Quote <= funds {
		// Buyer2 -> Seller Address
		sChanSeller.out.addressChannel <- Address("Jane Street")
		// Seller -> Buyer2 Date
		date := <-sChanSeller.in.dateChannel
		println("[Buyer2] date is ", date)
		endChannel <- true
	} else {
		// Buyer2 -> Seller Bye
		sChanSeller.out.byeChannel <- Bye{}
		endChannel <- true
	}
}

func seller(sChanBuyer1 *SessionChannel, sChanBuyer2 *SessionChannel, endChannel chan<- bool) {
	// Buyer1 -> Seller string
	title := <-sChanBuyer1.in.stringChannel
	println("[Seller] title is ", title)
	quote := 50
	// Seller -> Buyer 1 quote
	sChanBuyer1.out.intChannel <- quote
	// Seller -> Buyer 2 quote
	sChanBuyer2.out.intChannel <- quote
	// external choice
	select {
	// Buyer2 -> Seller Address
	case address := <-sChanBuyer2.in.addressChannel:
		println("[Seller] address is : ", address)
		date := Date("2018-01-20")
		// Seller -> Buyer2 Date
		sChanBuyer2.out.dateChannel <- date
		endChannel <- true
	// Buyer2 -> Seller Bye
	case <-sChanBuyer2.in.byeChannel:
		println("[Seller] got the bye message")
		endChannel <- true
	}
}

func main() {
	bufsize := 4
	sChanBuyer1Buyer2_1, sChanBuyer1Buyer2_2 := makeSessionChannelPair(bufsize)
	sChanBuyer1Seller_1, sChanBuyer1Seller_2 := makeSessionChannelPair(bufsize)
	sChanBuyer2Seller_1, sChanBuyer2Seller_2 := makeSessionChannelPair(bufsize)
	endChannel := make(chan bool)
	go buyer1(sChanBuyer1Buyer2_1, sChanBuyer1Seller_1, endChannel)
	go buyer2(sChanBuyer1Buyer2_2, sChanBuyer2Seller_1, endChannel)
	go seller(sChanBuyer1Seller_2, sChanBuyer2Seller_2, endChannel)
	// wait for parties to finish
	<-endChannel
	<-endChannel
	<-endChannel
	println("Exiting")
}
