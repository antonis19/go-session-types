package main

type Address string
type Date string
type Bye struct{}

type InputChannel struct {
	addressChannel <-chan Address
	dateChannel    <-chan Date
	byeChannel     <-chan Bye
}

type OutputChannel struct {
	addressChannel chan<- Address
	dateChannel    chan<- Date
	byeChannel     chan<- Bye
}

type Channel struct {
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

type SessionChannel struct {
	in  *InputChannel
	out *OutputChannel
}

func (channel *Channel) make(bufsize int) {
	channel.addressChannel = make(chan Address, bufsize)
	channel.dateChannel = make(chan Date, bufsize)
	channel.byeChannel = make(chan Bye, bufsize)
}

// Because Go does not allow casting of Channel to InChannel
// we define this function to make an InChannel copy of
// the given Channel
func (in *InputChannel) make(ch *Channel) {
	in.addressChannel = ch.addressChannel
	in.dateChannel = ch.dateChannel
	in.byeChannel = ch.byeChannel
}

// Because Go does not allow casting of Channel to OutChannel
// we define this function to make an OutChannel copy of
// the given Channel
func (out *OutputChannel) make(ch *Channel) {
	out.addressChannel = ch.addressChannel
	out.dateChannel = ch.dateChannel
	out.byeChannel = ch.byeChannel
}

func (schan *SessionChannel) make() {
	schan.in = new(InputChannel)
	schan.out = new(OutputChannel)
}

func makeSessionChannelPair(bufsize int) (schan1 *SessionChannel, schan2 *SessionChannel) {
	var in, out = new(Channel), new(Channel)
	schan1, schan2 = new(SessionChannel), new(SessionChannel)
	schan1.make()
	schan2.make()
	in.make(bufsize)
	out.make(bufsize)
	schan1.in.make(in)
	schan1.out.make(out)
	schan2.in.make(out) // schan1.out == schan1.in
	schan2.out.make(in) // schan1.in == schan2.out
	return
}

func Sender(schan *SessionChannel, endChannel chan bool) {
	funds, quote, buyer1quote := 30, 50, 25
	if quote-buyer1quote <= funds {
		schan.out.addressChannel <- Address("Main Street")
		date := <-schan.in.dateChannel
		println("date is ", date)
		endChannel <- true
	} else {
		schan.out.byeChannel <- Bye{}
		endChannel <- true
	}
}

func Receiver(schan *SessionChannel, endChannel chan bool) {
	select {
	case address := <-schan.in.addressChannel:
		println("address is ", address)
		schan.out.dateChannel <- Date("2018-01-20")
		endChannel <- true
	case <-schan.in.byeChannel:
		println("Received bye message")
		endChannel <- true
	}
}

func main() {
	schan1, schan2 := makeSessionChannelPair(4)
	endChannel := make(chan bool)
	go Sender(schan1, endChannel)
	go Receiver(schan2, endChannel)
	<-endChannel
	<-endChannel
}
