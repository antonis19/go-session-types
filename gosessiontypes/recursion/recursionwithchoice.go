package main

var count = 4


// Sender := !int . choice { recurse: Sender, quit : end    }
func Sender(ch chan int, endChannel chan bool) {
	// !int
	ch <- 42
	if count > 0  {
		count--
		// recurse
		Sender(ch,endChannel)
	} else {
		// quit
		endChannel <- true
	}
}

// Receiver := ?int . end
func Receiver(ch chan int, endChannel chan bool) {
	// ?int
	val := <-ch
	println("received ", val)
	// quit
	endChannel <- true
}


func main() {
	ch := make(chan int)
	endChannel := make(chan bool)
	go Sender(ch,endChannel)
	go Receiver(ch,endChannel)
	<-endChannel
	<-endChannel
}
