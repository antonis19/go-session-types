package main

// Sender := !int
func Sender(ch chan int) {
	// !int
	ch <- 42
	// ?int
	<-ch
	Sender(ch)
}

// Receiver := ?int . Receiver
func Receiver(ch chan int) {
	// ?int
	val := <-ch
	println("received ", val)
	// !int
	ch <- 299
	Receiver(ch)
}

func main() {
	ch := make(chan int)
	go Sender(ch)
	Receiver(ch)
}
