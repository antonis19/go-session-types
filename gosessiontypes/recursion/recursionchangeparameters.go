package main

var count = 4


// Sender := !int . choice { recurse: Sender, quit : end    }
func Sender(ch chan int, endChannel chan int) {
	// !int
	ch <- 42
	if count > 0  {
		count--
		// recurse
		Sender(ch,endChannel)
	} else {
		// quit
		endChannel <- 1
	}
}

// Receiver := ?int . end
func Receiver(ch chan int, endChannel chan int) {
	// ?int
	val := <-ch
	println("received ", val)
	// quit
	endChannel <- 1
}


func main() {
	ch := make(chan int)
	endChannel := make(chan int)
	go Sender(ch,endChannel)
	go Receiver(ch,endChannel)
	<-endChannel
	<-endChannel
}
