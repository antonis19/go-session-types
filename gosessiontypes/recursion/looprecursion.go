package main

// Sender := !int
func Sender(ch chan int) {
	ch <- 42
}

// Receiver := ?int . Receiver
func Receiver(ch chan int) {
	// ?int
	for {
		val := <-ch
		println("received ", val)
	}
}

func main() {
	ch := make(chan int)
	go Sender(ch)
	Receiver(ch)
}
