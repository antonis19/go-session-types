package main

// Sender := !int
func Sender(ch chan int) {
	ch <- 42
}

// Receiver := ?int . Receiver
func Receiver(ch chan int) {
	// ?int
	lab:
	val := <-ch
	println("received ", val)
	<-ch
	goto lab
}

func main() {
	ch := make(chan int)
	go Sender(ch)
	Receiver(ch)
}
