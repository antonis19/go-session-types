package main

import (
	"fmt"
	"log"
	"net"

	"bitbucket.org/antonis19/distchan"
)

var dive = 1

func serverRoutine(schan *SessionChannel, endChannel chan<- end) {
	fmt.Println("[serverRoutine] DIVE = ", dive)
	for i := 0; i < 4; i++ {
		fmt.Println("[serverRoutine] waiting for string.")
		str := schan.receiveString()
		fmt.Println("[serverRoutine] received : ", str)
		n := schan.receiveInt()
		fmt.Println("[serverRoutine] received : ", n)
	}
	n := schan.receiveInt()
	fmt.Println("[serverRoutine] received : ", n)
	// schan.sendBool(true)
	// dive++
	// serverRoutine(schan, endChannel)
	endChannel <- end{}
}

var ip = "192.168.178.30"

func makeServerConnection() net.Listener {
	ln, err := net.Listen("tcp", ip+":5678")
	if err != nil {
		log.Fatalln(err)
	}
	return ln
}

func main() {
	bufsize := 4 // choose an  arbitary buffer size
	sChanBuyer2Seller_2, _ := newSessionChannelPair(bufsize)
	endChannel := make(chan end)
	ln := makeServerConnection()
	server, err := distchan.NewServer(ln, sChanBuyer2Seller_2)
	if err != nil {
		panic("could not create new server")
	}
	server.Start()
	server.WaitUntilReady()
	go serverRoutine(sChanBuyer2Seller_2, endChannel)
	println("Goroutine started")
	// Wait for goroutines to finish
	<-endChannel
	server.Done()
	//select {}
	// fmt.Println("server.Done() called")
}
