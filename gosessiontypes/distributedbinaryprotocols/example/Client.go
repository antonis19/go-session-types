package main

import (
	"fmt"
	"log"
	"net"

	"bitbucket.org/antonis19/distchan"
)

var bigString = "Hello World"

var dive = 1

func clientRoutine(schan *SessionChannel, endChannel chan<- end) {
	fmt.Println("[clientRoutine] DIVE = ", dive)
	for i := 0; i < 4; i++ {
		schan.sendString(bigString)
		schan.sendInt(99)
	}
	schan.sendInt(42)
	b := schan.receiveBool()
	fmt.Println("[clientRoutine] received : ", b)
	dive++
	clientRoutine(schan, endChannel)
	endChannel <- end{}
}

var ip = `192.168.178.27`

func makeConnection() net.Conn {
	conn, err := net.Dial("tcp", ip+":5678") // must be able to connect to the server
	if err != nil {
		log.Fatalln(err)
	}
	return conn
}

func main() {
	bufsize := 4 // choose an  arbitary buffer size
	schan, _ := newSessionChannelPair(bufsize)
	endChannel := make(chan end)
	conn := makeConnection()
	client, err := distchan.NewClient(conn, schan)
	if err != nil {
		panic("Could not create client")
	}
	client.Start()
	go clientRoutine(schan, endChannel)
	// Wait for goroutines to finish
	<-endChannel
	client.Done()
}
