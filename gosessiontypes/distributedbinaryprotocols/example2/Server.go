package main

import (
	"fmt"
	"log"
	"net"
	"strconv"

	. "./schan"
	"bitbucket.org/antonis19/distchan"
)

type end struct{}

var SERVER_HOST = `localhost`
var SERVER_PORT = 3333

func createListener(host string, port int) net.Listener {
	ln, err := net.Listen("tcp", host+":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalln(err)
	}
	return ln
}

func SMTPServer(schanClient *SessionChannel, endChannel chan end) {
	for i := 0; i < 10; i++ {
		val := schanClient.ReceiveInt()
		fmt.Println("Val = ", val)
	}
	for i := 0; i < 10; i++ {
		schanClient.SendInt(i)
	}
	endChannel <- end{}
}

func main() {
	bufsize := 1 // pick an arbitary buffer size
	schan, _ := NewSessionChannelPair(bufsize)
	endChannel := make(chan end)
	ln := createListener(SERVER_HOST, SERVER_PORT)
	server, err := distchan.NewServer(ln, schan)
	if err != nil {
		log.Fatalln("Could not create new server")
	}
	server.Start()
	server.WaitUntilReady()
	go SMTPServer(schan, endChannel)
	<-endChannel
	server.Done()
}
