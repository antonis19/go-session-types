package main

import (
	"fmt"
	"log"
	"net"
	"strconv"

	. "./schan"
	"bitbucket.org/antonis19/distchan"
)

type end struct{}

var SERVER_HOST = `localhost`
var SERVER_PORT = 3333

func connect(host string, port int) net.Conn {
	conn, err := net.Dial("tcp", host+":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalln(err)
	}
	return conn
}

func SMTPClient(schanServer *SessionChannel, endChannel chan end) {
	for i := 0; i < 10; i++ {
		schanServer.SendInt(i)
	}
	for i := 0; i < 10; i++ {
		val := schanServer.ReceiveInt()
		fmt.Println("Val = ", val)
	}
	endChannel <- end{}
}

func main() {
	bufsize := 1 // pick an arbitary buffer size
	schan, _ := NewSessionChannelPair(bufsize)
	endChannel := make(chan end)
	conn := connect(SERVER_HOST, SERVER_PORT)
	client, err := distchan.NewClient(conn, schan)
	if err != nil {
		log.Fatalln("Could not create new client")
	}
	client.Start()
	go SMTPClient(schan, endChannel)
	<-endChannel
	client.Done()
}
