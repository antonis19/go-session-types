package schan

import (
	"reflect"
)

// Custom datatypes
type Domain string
type FromAddress string
type ToAddress string
type Message string
type HelpMessage string

type Label struct{}

// Labels
type EHLO Label
type HELP Label
type SIZE Label
type QUIT Label

type SessionChannel struct {
	In  *Channels
	Out *Channels
}

// Channels contains channels for all datatypes that will be exchanged
// throughout the session
type Channels struct {
	// datatype channels
	IntChannel         chan int
	DomainChannel      chan Domain
	FromAddressChannel chan FromAddress
	ToAddressChannel   chan ToAddress
	MessageChannel     chan Message
	HelpMessageChannel chan HelpMessage
	// Label channels
	EHLOChannel chan EHLO
	HELPChannel chan HELP
	SIZEChannel chan SIZE
	QUITChannel chan QUIT
}

func (channels *Channels) make(bufsize int) {
	// Make datatype channels
	channels.IntChannel = make(chan int, bufsize)
	channels.DomainChannel = make(chan Domain, bufsize)
	channels.FromAddressChannel = make(chan FromAddress, bufsize)
	channels.ToAddressChannel = make(chan ToAddress, bufsize)
	channels.MessageChannel = make(chan Message, bufsize)
	channels.HelpMessageChannel = make(chan HelpMessage, bufsize)
	// Make Label channels
	channels.EHLOChannel = make(chan EHLO, bufsize)
	channels.HELPChannel = make(chan HELP, bufsize)
	channels.SIZEChannel = make(chan SIZE, bufsize)
	channels.QUITChannel = make(chan QUIT, bufsize)
}

func (schan *SessionChannel) make(bufsize int) {
	schan.In = new(Channels)
	schan.Out = new(Channels)
	schan.In.make(bufsize)
	schan.Out.make(bufsize)
}

// NewSessionChannelPair returns a pair of SessionChannels that share the same
// underlying channels, just with different names
func NewSessionChannelPair(bufsize int) (*SessionChannel, *SessionChannel) {
	schan1 := new(SessionChannel)
	schan1.make(bufsize)
	schan2 := new(SessionChannel)
	schan2.In = new(Channels)
	schan2.Out = new(Channels)
	// schan1 and schan2 share the same channels, just with different names
	schan2.In = schan1.Out
	schan2.Out = schan1.In
	return schan1, schan2
}

// An alternative to calling the function newSessionChannelPair
// is to do schan2 := schan1.dual()
func (schan1 *SessionChannel) Dual() *SessionChannel {
	schan2 := new(SessionChannel)
	schan2.In = new(Channels)
	schan2.Out = new(Channels)
	schan2.In = schan1.Out
	schan2.Out = schan1.In
	return schan2
}

// func (schan *SessionChannel) GetInputChannels() []interface{} {
// 	return []interface{schan.In.DomainChannel,schan.In.EHLOChannel,
// 	schan.In.FromAddressChannel,schan.In.HELPChannel,schan.In.HelpMessageChannel,
// 	schan.In.IntChannel,schan.In.MessageChannel,schan.In.QUITChannel}
// }

func (schan *SessionChannel) GetInputChannels() []interface{} {
	return getFields(*schan.In)
}

func (schan *SessionChannel) GetOutputChannels() []interface{} {
	return getFields(*schan.Out)
}

func getFields(i interface{}) []interface{} {
	v := reflect.ValueOf(i)
	fields := make([]interface{}, v.NumField())
	for i := 0; i < v.NumField(); i++ {
		fields[i] = v.Field(i).Interface()
	}
	return fields
}

// Sends

func (schan *SessionChannel) SendDomain(d Domain) {
	schan.Out.DomainChannel <- d
}

func (schan *SessionChannel) SendFromAddress(from FromAddress) {
	schan.Out.FromAddressChannel <- from
}

func (schan *SessionChannel) SendToAddress(to ToAddress) {
	schan.Out.ToAddressChannel <- to
}

func (schan *SessionChannel) SendMessage(msg Message) {
	schan.Out.MessageChannel <- msg
}

func (schan *SessionChannel) SendHelpMessage(help HelpMessage) {
	schan.Out.HelpMessageChannel <- help
}

func (schan *SessionChannel) SendInt(n int) {
	schan.Out.IntChannel <- n
}

func (schan *SessionChannel) SendEHLO(ehlo EHLO) {
	schan.Out.EHLOChannel <- ehlo
}

func (schan *SessionChannel) SendHELP(help HELP) {
	schan.Out.HELPChannel <- help
}

func (schan *SessionChannel) SendSIZE(size SIZE) {
	schan.Out.SIZEChannel <- size
}

func (schan *SessionChannel) SendQUIT(quit QUIT) {
	schan.Out.QUITChannel <- quit
}

// Receives
func (schan *SessionChannel) ReceiveDomain() Domain {
	return <-schan.In.DomainChannel
}

func (schan *SessionChannel) ReceiveFromAddress() FromAddress {
	return <-schan.In.FromAddressChannel
}

func (schan *SessionChannel) ReceiveToAddress() ToAddress {
	return <-schan.In.ToAddressChannel
}

func (schan *SessionChannel) ReceiveMessage() Message {
	return <-schan.In.MessageChannel
}

func (schan *SessionChannel) ReceiveHelpMessage() HelpMessage {
	return <-schan.In.HelpMessageChannel
}

func (schan *SessionChannel) ReceiveInt() int {
	return <-schan.In.IntChannel
}

func (schan *SessionChannel) ReceiveEHLO() EHLO {
	return <-schan.In.EHLOChannel
}

func (schan *SessionChannel) ReceiveHELP() HELP {
	return <-schan.In.HELPChannel
}

func (schan *SessionChannel) ReceiveSIZE() SIZE {
	return <-schan.In.SIZEChannel
}

func (schan *SessionChannel) ReceiveQUIT() QUIT {
	return <-schan.In.QUITChannel
}
