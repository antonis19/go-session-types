package main

import (
	"fmt"
	"log"
	"milton19/distchan"
	"net"
)

// The data types that will be exchanged
type Address struct {
	Street string
	Number int
}
type Date string
type Bye struct{}

type Label struct{}

// The two labels used in the protocol
type OK Label
type QUIT Label

type end struct{}

// Channels is a struct encapsulating the channels for datatypes that will be exchanged
type Channels struct {
	stringChannel  chan string
	intChannel     chan int
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
	// Label channels
	okChannel   chan OK
	quitChannel chan QUIT
}

// SessionChannel
type SessionChannel struct {
	in  *Channels
	out *Channels
}

func (channels *Channels) make(bufsize int) {
	channels.stringChannel = make(chan string, bufsize)
	channels.intChannel = make(chan int, bufsize)
	channels.addressChannel = make(chan Address, bufsize)
	channels.dateChannel = make(chan Date, bufsize)
	channels.byeChannel = make(chan Bye, bufsize)
	// make label channels
	channels.okChannel = make(chan OK, bufsize)
	channels.quitChannel = make(chan QUIT, bufsize)
}

func (schan *SessionChannel) make(bufsize int) {
	schan.in = new(Channels)
	schan.out = new(Channels)
	schan.in.make(bufsize)
	schan.out.make(bufsize)
}

// This function returns a pair of SessionChannels that share the same
// underlying channels, just with different names
func newSessionChannelPair(bufsize int) (*SessionChannel, *SessionChannel) {
	schan1 := new(SessionChannel)
	schan1.make(bufsize)
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	// schan1 and schan2 share the same channels, just with different names
	schan2.in = schan1.out
	schan2.out = schan1.in
	return schan1, schan2
}

// An alternative to calling the function newSessionChannelPair
// is to do schan2 := schan1.dual()
func (schan1 *SessionChannel) dual() *SessionChannel {
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	schan2.in = schan1.out
	schan2.out = schan1.in
	return schan2
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetInputChannels() []interface{} {
	return []interface{}{schan.in.stringChannel, schan.in.intChannel,
		schan.in.addressChannel, schan.in.dateChannel,
		schan.in.okChannel, schan.in.byeChannel}
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetOutputChannels() []interface{} {
	return []interface{}{schan.out.stringChannel, schan.out.intChannel,
		schan.out.addressChannel, schan.out.dateChannel,
		schan.out.okChannel, schan.out.byeChannel}
}

// Send/Receive operations on datatypes hide implementation details
// We cannot do the same thing for labels because select-case requires explicit
// channel operations on the cases

// Sends

func (schan *SessionChannel) sendString(str string) {
	schan.out.stringChannel <- str
}

func (schan *SessionChannel) sendInt(n int) {
	schan.out.intChannel <- n
}

func (schan *SessionChannel) sendAddress(address Address) {
	schan.out.addressChannel <- address
}

func (schan *SessionChannel) sendDate(date Date) {
	schan.out.dateChannel <- date
}

func (schan *SessionChannel) sendBye(bye Bye) {
	schan.out.byeChannel <- bye
}

//Receives

func (schan *SessionChannel) receiveString() string {
	return <-schan.in.stringChannel
}

func (schan *SessionChannel) receiveInt() int {
	return <-schan.in.intChannel
}

func (schan *SessionChannel) receiveAddress() Address {
	return <-schan.in.addressChannel
}

func (schan *SessionChannel) receiveDate() Date {
	return <-schan.in.dateChannel
}

func (schan *SessionChannel) receiveBye() Bye {
	return <-schan.in.byeChannel
}
func seller(sChanBuyer2 *SessionChannel, endChannel chan<- end) {
	fmt.Println("[Seller] started running")
	quote := 50
	// Seller -> Buyer 2 quote
	sChanBuyer2.sendInt(quote)
	// external choice
	select {
	case <-sChanBuyer2.in.okChannel:
		// Buyer2 -> Seller Address
		address := sChanBuyer2.receiveAddress()
		fmt.Println("[Seller] address is : ", address)
		date := Date("2018-01-20")
		// Seller -> Buyer2 Date
		sChanBuyer2.sendDate(date)
		//str := sChanBuyer2.receiveString()
		//fmt.Println("[Seller] str = ", str)
		endChannel <- end{}
	// Buyer2 -> Seller Bye
	case <-sChanBuyer2.in.quitChannel:
		sChanBuyer2.receiveBye()
		fmt.Println("[Seller] got the bye message")
		endChannel <- end{}
	}
}

func makeServerConnection() net.Listener {
	ln, err := net.Listen("tcp", "localhost:5678")
	if err != nil {
		log.Fatalln(err)
	}
	return ln
}

func main() {
	bufsize := 4 // choose an  arbitary buffer size
	sChanBuyer2Seller_2, _ := newSessionChannelPair(bufsize)
	endChannel := make(chan end)
	ln := makeServerConnection()
	server, err := distchan.NewServer(ln, sChanBuyer2Seller_2)
	if err != nil {
		panic("could not create new server")
	}
	server.Start()
	server.WaitUntilReady()
	go seller(sChanBuyer2Seller_2, endChannel)
	println("Goroutine started")
	// Wait for goroutines to finish
	<-endChannel
	server.Done()
	//select {}
	// fmt.Println("server.Done() called")
}
