package main

type SessionChannel struct {
	intChannel    chan int
	boolChannel   chan bool
	stringChannel chan string
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel {
		intChannel : make(chan int),
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
	}
}

var x = 299


func internalChoice(schan *SessionChannel) {
	if x < 42 {
		// !int
		schan.intChannel <- 99 
	} else if x > 42 {
		// ?string
		<-schan.stringChannel
	} else {
		// !bool
		schan.boolChannel <- true
	}
}


func externalChoice(schan *SessionChannel) {
	select {
		case <-schan.intChannel : // ?int
			println("Received an int")
		case schan.stringChannel <- "Hello World" : // !string
			println("Sent a string")
		case <-schan.boolChannel : // ?bool
			println("Received a bool")
	}
}


func main() {
	schan := makeSessionChannel()
	go externalChoice(schan)
	internalChoice(schan)
}
