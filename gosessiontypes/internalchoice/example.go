package main

type Address string
type Date string
type Bye struct{}

func Sender(addressChannel chan Address, dateChannel chan Date, byeChannel chan Bye, endChannel chan bool) {
	funds, quote, buyer1quote := 30, 50, 25
	if quote-buyer1quote <= funds {
		addressChannel <- Address("Main Street")
		date := <-dateChannel
		println("date is ", date)
		endChannel <- true
	} else {
		byeChannel <- Bye{}
		endChannel <- true
	}
}

func Receiver(addressChannel chan Address, dateChannel chan Date, byeChannel chan Bye, endChannel chan bool) {
	select {
	case address := <-addressChannel:
		println("address is ", address)
		dateChannel <- Date("2018-01-20")
		endChannel <- true
	case <-byeChannel:
		println("Received bye message")
		endChannel <- true
	}
}

func main() {
	addressChannel, dateChannel, byeChannel := make(chan Address), make(chan Date), make(chan Bye)
	endChannel := make(chan bool)
	go Sender(addressChannel, dateChannel, byeChannel, endChannel)
	go Receiver(addressChannel, dateChannel, byeChannel, endChannel)
	// Wait for goroutines to finish
	<-endChannel
	<-endChannel
}
