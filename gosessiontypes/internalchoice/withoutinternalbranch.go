package main

func Sender(intChannel chan int, stringChannel chan string, endChannel chan bool) {
	// * {"int": !int . end }
	intChannel <- 42
	endChannel <- true
}

func Receiver(intChannel chan int, stringChannel chan string, endChannel chan bool) {
	// & {"int" ?int . end , "string" : ?string . end}
	select {
	case <-intChannel:
		endChannel <- true
	case <-stringChannel:
		endChannel <- true
	}
}

func main() {
	intChannel, stringChannel := make(chan int), make(chan string)
	endChannel := make(chan bool)
	go Sender(intChannel, stringChannel, endChannel)
	go Receiver(intChannel, stringChannel, endChannel)
	<-endChannel
	<-endChannel
}
