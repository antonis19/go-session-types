package main

type Label string

type A = Label
type B = Label

var x = 42

func Sender(labelChannel chan Label, intChannel chan int, stringChannel chan string, endChannel chan bool) {
	// * {"A" : !int . end, "B" : !string .end}
	if x == 42 {
		labelChannel <- A("int")
		intChannel <- 42
		endChannel <- true
	} else {
		labelChannel <- B("string")
		stringChannel <- "Hello World"
		endChannel <- true
	}
}

func Receiver(labelChannel chan Label, intChannel chan int, stringChannel chan string, endChannel chan bool) {
	// & {"int" : ?int . end , "string" : ?string.end }
	label := <-labelChannel
	if label == "int" {
		<-intChannel
		endChannel <- true
	} else if label == "string" {
		<-stringChannel
		endChannel <- true
	} else {
		panic("Unexpected choice")
	}
}

func main() {
	labelChannel := make(chan Label)
	intChannel, stringChannel := make(chan int), make(chan string)
	endChannel := make(chan bool)
	go Sender(labelChannel, intChannel, stringChannel, endChannel)
	go Receiver(labelChannel, intChannel, stringChannel, endChannel)
	<-endChannel
	<-endChannel
}
