package main

type Address string

type Date string

type Bye struct{}

type SessionChannel struct {
	boolChannel    chan bool
	stringChannel  chan string
	intChannel     chan int
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel{
		boolChannel:    make(chan bool),
		stringChannel:  make(chan string),
		intChannel:     make(chan int),
		addressChannel: make(chan Address),
		dateChannel:    make(chan Date),
		byeChannel:     make(chan Bye),
	}
}

func buyer2(Seller *SessionChannel, end chan<- bool) {
	funds := 30
	// Seller -> Buyer2 int
	//quote := <-Seller.intChannel
	quote := 50
	println("[Buyer2] quote is ", quote)
	// Buyer1 -> Buyer2 int
	// SKIP
	buyer1Quote := 25 // hardcode what would normally be received from Buyer1
	// internal choice
	if quote-buyer1Quote <= funds {
		// Buyer2 -> Seller Address
		Seller.addressChannel <- Address("Jane Street")
		// Seller -> Buyer2 Date
		date := <-Seller.dateChannel
		println("[Buyer2] date is ", date)
		end <- true
	} else {
		// Buyer2 -> Seller Bye
		Seller.byeChannel <- Bye{}
		end <- true
	}
}

func seller(Buyer2 *SessionChannel, end chan<- bool) {
	// Buyer1 -> Seller string
	// SKIP
	//quote:= 50
	// Seller -> Buyer 1 quote
	// SKIP
	// Seller -> Buyer 2 quote
	//Buyer2.intChannel <- quote
	// external choice
	select {
	// Buyer2 -> Seller Address
	case address := <-Buyer2.addressChannel:
		println("[Seller] address is : ", address)
		date := Date("2018-01-20")
		// Seller -> Buyer2 Date
		Buyer2.dateChannel <- date
		end <- true
	// Buyer2 -> Seller Bye
	case <-Buyer2.byeChannel:
		println("[Seller] got the bye message ")
		end <- true
	}
}

func main() {
	Buyer2Seller := &SessionChannel{
		boolChannel:    make(chan bool),
		stringChannel:  make(chan string),
		intChannel:     make(chan int),
		addressChannel: make(chan Address),
		dateChannel:    make(chan Date),
		byeChannel:     make(chan Bye),
	}
	end := make(chan bool)
	go buyer2(Buyer2Seller, end)
	go seller(Buyer2Seller, end)
	// wait for parties to finish
	<-end
	<-end
	println("Exiting")
}
