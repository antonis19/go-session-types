package main


type Address struct {
	addressField string
}

type Date struct {
	dateField string
}

type Bye struct{}

type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
	addressChannel chan Address
	dateChannel chan Date
	byeChannel chan Bye
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
}


func buyer1(Buyer2 *SessionChannel, end chan <- bool) {
	// Buyer1 -> Seller string
	// SKIP
	// Seller -> Buyer1 int
	// SKIP
	// Buyer1 -> Buyer2 string
	quote := 50 // hardcode what would be received from Seller
	Buyer2.intChannel <- quote/2
	// end
	end <- true
}


func buyer2(Buyer1 *SessionChannel,  end chan <- bool) {
	// Seller -> Buyer2 int
	// SKIP 
	// Buyer1 -> Buyer2 int
	buyer1Quote := <-Buyer1.intChannel
	println("[Buyer2] buyer1Quote is", buyer1Quote)
	// internal choice
	// SKIP
	end <- true
}

func main() {
	Buyer1Buyer2 := &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
	end :=  make(chan bool)
	go buyer1(Buyer1Buyer2,end)
	go buyer2(Buyer1Buyer2,end)
	// wait for parties to finish
	<-end
	<-end
	println("Exiting")
}
