package main


type Address struct {
	addressField string
}

type Date struct {
	dateField string
}

type Bye struct{}

type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
	addressChannel chan Address
	dateChannel chan Date
	byeChannel chan Bye
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
}


func buyer1(Seller *SessionChannel, end chan <- bool) {
	title := "The Go Programming Language"
	// Buyer1 -> Seller string
	Seller.stringChannel <- title
	// Seller -> Buyer1 int
	quote:= <-Seller.intChannel
	println("[Buyer1] quote is ",quote)
	// Buyer1 -> Buyer2 string
	// SKIP
	// end
	end <- true
}


func seller(Buyer1 *SessionChannel, end chan <- bool) {
	// Buyer1 -> Seller string
	title := <-Buyer1.stringChannel
	println("[Seller] title is ",title)
	quote:= 50
	// Seller -> Buyer 1 quote
	Buyer1.intChannel <- quote
	// Seller -> Buyer 2 quote
	// SKIP
	// external choice
	// SKIP	
	end <- true
}

func main() {	
	Buyer1Seller := &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
	
	end :=  make(chan bool)
	go buyer1(Buyer1Seller,end)
	go seller(Buyer1Seller,end)
	// wait for parties to finish
	<-end
	<-end
	println("Exiting")
}
