package main


type Address struct {
	addressField string
}

type Date struct {
	dateField string
}

type Bye struct{}

type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
	addressChannel chan Address
	dateChannel chan Date
	byeChannel chan Bye
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
}


func buyer1(Buyer2 *SessionChannel, Seller *SessionChannel, end chan <- bool) {
	title := "The Go Programming Language"
	// Buyer1 -> Seller string
	Seller.stringChannel <- title
	// Seller -> Buyer1 int
	quote:= <-Seller.intChannel
	println("[Buyer1] quote is ",quote)
	// Buyer1 -> Buyer2 string
	Buyer2.intChannel <- quote/2
	// end
}


func buyer2(Buyer1 *SessionChannel, Seller *SessionChannel, end chan <- bool) {
	funds := 30
	// Seller -> Buyer2 int
	quote := <-Seller.intChannel
	println("[Buyer2] quote is ",quote)
	// Buyer1 -> Buyer2 int
	buyer1Quote := <-Buyer1.intChannel
	println("[Buyer2] buyer1Quote is", buyer1Quote)
	// internal choice
	if quote - buyer1Quote <= funds {
		// Buyer2 -> Seller Address
		Seller.addressChannel <- Address { addressField: "Jane Street",}
		// Seller -> Buyer2 Date
		date := <-Seller.dateChannel
		println("[Buyer2] date is ", date.dateField)
	} else {
		// Buyer2 -> Seller Bye
		Seller.byeChannel <- Bye{}
	}
	//end <- true
		
}

func seller(Buyer1 *SessionChannel, Buyer2 *SessionChannel, end chan <- bool) {
	// Buyer1 -> Seller string
	title := <-Buyer1.stringChannel
	println("[Seller] title is ",title)
	quote:= 50
	// Seller -> Buyer 1 quote
	Buyer1.intChannel <- quote
	// Seller -> Buyer 2 quote
	Buyer2.intChannel <- quote
	// external choice
	select {
		// Buyer2 -> Seller Address
		case address:= <-Buyer2.addressChannel :
			println("[Seller] address is : ",address.addressField)
			date:= Date{ dateField : "2018-01-20"}
			// Seller -> Buyer2 Date
			Buyer2.dateChannel <- date
		// Buyer2 -> Seller Bye
		case  <-Buyer2.byeChannel :
			println("[Seller] got the bye message")
	} 		
	//end <- true
}

func main() {
	const nParties = 3
	//Buyer1Buyer2, Buyer1Seller, Buyer2Seller := makeSessionChannel(), makeSessionChannel(), makeSessionChannel()
	Buyer1Buyer2 := &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
	
	Buyer1Seller := &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
	
	Buyer2Seller := &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
	end :=  make(chan bool)
	go buyer1(Buyer1Buyer2,Buyer1Seller,end)
	go buyer2(Buyer1Buyer2,Buyer2Seller,end)
	seller(Buyer1Seller,Buyer2Seller,end)
	// wait for parties to finish
	println("Exiting")
}
