package main

import (
	"math/rand"
	"time"
)

func nonBlockingReceiveInt(ch <- chan int) (int, bool) {
	var val int;
	select {
		case val = <- ch :
			return val, true
		default :
			return val, false
	}
}

func nonBlockingReceiveString(ch <- chan string) (string, bool) {
	var val string;
	select {
		case val = <- ch :
			return val, true
		default :
			return val, false
	}
}

func selectCases(intChannel <- chan int, stringChannel <- chan string,endChannel chan bool) {
	if val,ok := nonBlockingReceiveInt(intChannel) ; ok {
			println("Received ",val)
			endChannel <- true
			return;
	} else if val, ok := nonBlockingReceiveString(stringChannel) ; ok {
			println("Received ",val)
			endChannel <- true
			return;
	} else {
		selectCases(intChannel,stringChannel)
	}
}


func Sender(intChannel chan <- int , stringChannel chan <- string, endChannel chan <- bool) {
	randInt := rand.Int()
	if randInt%2 == 0 {
		intChannel <- randInt
		endChannel <- true
	} else  {
		stringChannel <- "odd"
		endChannel <- true
	}
}

func Receiver(intChannel <- chan  int , stringChannel <- chan  string, endChannel chan <- bool) {
	selectCases(intChannel,stringChannel)
	/*for {
		if val,ok := nonBlockingReceiveInt(intChannel) ; ok {
			println("Received ",val)
			break;
		} else if val, ok := nonBlockingReceiveString(stringChannel) ; ok {
			println("Received ",val)
			break;
		}
	}*/
	/*select {
		case val:= <-intChannel :
			println("Received ",val)
		case val := <-stringChannel :
			println("Received ",val)
	}*/
	endChannel <- true
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	intChannel, stringChannel, endChannel := make(chan int), make(chan string), make(chan bool)
	go Sender(intChannel,stringChannel,endChannel)
	go Receiver(intChannel,stringChannel,endChannel)
	<-endChannel
	<-endChannel
}
