package main

type SessionChannel struct {
	intChannel    chan int
	boolChannel   chan bool
	stringChannel chan string
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel {
		intChannel : make(chan int),
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
	}
}

func (schan *SessionChannel) make() {
	schan.intChannel = make(chan int)
	schan.boolChannel = make(chan bool)
	schan.stringChannel = make(chan string)
}


var x = 299


func internalChoice(schan *SessionChannel) {
	if x < 42 {
		// !int
		schan.intChannel <- 99 
	} else if x > 42 {
		// ?string
		<-schan.stringChannel
	} else {
		// !bool
		schan.boolChannel <- true
	}
}


func externalChoice(schan *SessionChannel) {
	if x > 42 {
		// !string
		schan.stringChannel <- "Hello World"
	} else if x < 42 {
		// ?int
		<-schan.intChannel
	} else {
		// ?bool
		<-schan.boolChannel
	}
}

func main() {
	schan := new(SessionChannel)
	schan.make()
	go externalChoice(schan)
	internalChoice(schan)
}
