package main

import (
	"math/rand"
	"time"
)

func Sender(intChannel chan <- int , stringChannel chan <- string, endChannel chan <- bool) {
	randInt := rand.Int()
	if randInt%2 == 0 {
		intChannel <- randInt
	} else  {
		stringChannel <- "odd"
	}
	endChannel <- true
}

func Receiver(intChannel <- chan  int , stringChannel <- chan  string, endChannel chan <- bool) {
	select {
		case val:= <-intChannel :
			println("Received ",val)
		case val := <-stringChannel :
			println("Received ",val)
	}
	endChannel <- true
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	intChannel, stringChannel, endChannel := make(chan int), make(chan string), make(chan bool)
	go Sender(intChannel,stringChannel,endChannel)
	go Receiver(intChannel,stringChannel,endChannel)
	<-endChannel
	<-endChannel
}
