package main


type Label string

func Sender(labelChannel chan Label, intChannel chan int, stringChannel chan string, endChannel chan bool) {
	// * {"int" : !int . end , "string" : !string . end}
	if true {
		labelChannel <- "int"
		intChannel <- 42
		endChannel <- true
	} else {
		labelChannel <- "string"
		stringChannel <- "Hello World"
		endChannel <- true
	}
}

func Receiver(labelChannel chan Label,intChannel chan int, stringChannel chan string, endChannel chan bool) {
	// & {"int" : ?int . end }
	label := <-labelChannel
	if label == "int" {
		<-intChannel
		endChannel <- true
	}
}

func main() {
	labelChannel := make(chan Label)
	intChannel, stringChannel := make(chan int), make(chan string)
	endChannel := make(chan bool)
	go Sender(labelChannel,intChannel, stringChannel, endChannel)
	go Receiver(labelChannel,intChannel, stringChannel,endChannel)
	<-endChannel
	<-endChannel
}
