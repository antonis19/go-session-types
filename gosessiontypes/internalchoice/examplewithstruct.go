package main

type Address string
type Date string
type Bye struct{}

type SessionChannel struct {
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

func Sender(schan *SessionChannel, endChannel chan bool) {
	funds, quote, buyer1quote := 30, 50, 25
	if quote-buyer1quote <= funds {
		schan.addressChannel <- Address("Main Street")
		date := <-schan.dateChannel
		println("date is ", date)
		endChannel <- true
	} else {
		schan.byeChannel <- Bye{}
		endChannel <- true
	}
}

func Receiver(schan *SessionChannel, endChannel chan bool) {
	select {
	case address := <-schan.addressChannel:
		println("address is ", address)
		schan.dateChannel <- Date("2018-01-20")
		endChannel <- true
	case <-schan.byeChannel:
		println("Received bye message")
		endChannel <- true
	}
}

func main() {
	schan := &SessionChannel{
		addressChannel: make(chan Address),
		dateChannel:    make(chan Date),
		byeChannel:     make(chan Bye),
	}
	endChannel := make(chan bool)
	go Sender(schan, endChannel)
	go Receiver(schan, endChannel)
	// Wait for goroutines to finish
	<-endChannel
	<-endChannel
}
