package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"reflect"
)

type Point struct {
	X, Y int
}

type Address struct {
	Street string
	Number int
}

// This example shows how to encode an interface value. The key
// distinction from regular types is to register the concrete type that
// implements the interface.
func main() {
	var network bytes.Buffer // Stand-in for the network.

	// We must register the concrete type for the encoder and decoder (which would
	// normally be on a separate machine from the encoder). On each end, this tells the
	// engine which concrete type is being sent that implements the interface.
	gob.Register(Point{})

	// Create an encoder and send a value
	enc := gob.NewEncoder(&network)
	var channel chan Address
	chanElem := reflect.TypeOf(channel).Elem()
	gob.Register(reflect.New(chanElem).Elem().Interface())
	var x = Address{"Main Street", 42} // The value to send
	var iface interface{} = reflect.ValueOf(x).Interface()
	err := enc.Encode(&iface)
	if err != nil {
		panic(err)
	}
	// Create a decoder and receive some values.
	var result interface{}
	dec := gob.NewDecoder(&network)
	err = dec.Decode(&result)
	if err != nil {
		panic(err)
	}

	if result == nil {
		println("nil interface")
	}
	fmt.Println("Result is ", reflect.ValueOf(result))

}

// interfaceEncode encodes the interface value into the encoder.
func interfaceEncode(enc *gob.Encoder, p interface{}) {
	// The encode will fail unless the concrete type has been
	// registered. We registered it in the calling function.

	// Pass pointer to interface so Encode sees (and hence sends) a value of
	// interface type. If we passed p directly it would see the concrete type instead.
	// See the blog post, "The Laws of Reflection" for background.
	err := enc.Encode(&p)
	if err != nil {
		log.Fatal("encode:", err)
	}
}

// interfaceDecode decodes the next interface value from the stream and returns it.
func interfaceDecode(dec *gob.Decoder) interface{} {
	// The decode will fail unless the concrete type on the wire has been
	// registered. We registered it in the calling function.
	var p interface{}
	err := dec.Decode(&p)
	if err != nil {
		log.Fatal("decode:", err)
	}
	return p
}
