package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"reflect"
)

func main() {
	var network bytes.Buffer // Stand-in for the network.
	enc := gob.NewEncoder(&network)
	var x = 42 // The value to send
	var iface interface{} = reflect.ValueOf(x).Interface()
	err := enc.Encode(iface)
	if err != nil {
		panic("error in encoding")
	}
	// Create a decoder and receive some values.
	var result interface{}
	dec := gob.NewDecoder(&network)
	err = dec.Decode(result)
	if err != nil {
		panic("error in decoding")
	}

	if result == nil {
		println("nil interface")
	}
	fmt.Println("Result is ", reflect.ValueOf(result))
}
