package schan

import (
	"time"
)

type Address struct {
	StreetName   string
	StreetNumber int
}
type Date struct {
	Year  int
	Month time.Month
	Day   int
}

type Accept struct{}
type Reject struct{}

type Label struct{}

// Labels
type OK Label
type QUIT Label
type QUERY Label

type SessionChannel struct {
	in  *Channels
	out *Channels
	// Label channels
	OKChannel    chan OK
	QUITChannel  chan QUIT
	QUERYChannel chan QUERY
}

// Channels contains channels for all datatypes that will be exchanged
// throughout the session
type Channels struct {
	IntChannel     chan int
	StringChannel  chan string
	AddressChannel chan Address
	DateChannel    chan Date
	AcceptChannel  chan Accept
	RejectChannel  chan Reject
}

func (channels *Channels) make(bufsize int) {
	channels.IntChannel = make(chan int, bufsize)
	channels.StringChannel = make(chan string, bufsize)
	channels.AddressChannel = make(chan Address, bufsize)
	channels.DateChannel = make(chan Date, bufsize)
	channels.AcceptChannel = make(chan Accept, bufsize)
	channels.RejectChannel = make(chan Reject, bufsize)
}

func (schan *SessionChannel) make(bufsize int) {
	schan.in = new(Channels)
	schan.out = new(Channels)
	schan.in.make(bufsize)
	schan.out.make(bufsize)
	// make label channels
	schan.OKChannel = make(chan OK)
	schan.QUITChannel = make(chan QUIT)
	schan.QUERYChannel = make(chan QUERY)
}

// NewSessionChannelPair returns a pair of SessionChannels that share the same
// underlying channels, just with different names
func NewSessionChannelPair(bufsize int) (*SessionChannel, *SessionChannel) {
	schan1 := new(SessionChannel)
	schan1.make(bufsize)
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	// schan1 and schan2 share the same channels, just with different names
	schan2.in = schan1.out
	schan2.out = schan1.in
	// these channels are for sending/receiving labels
	schan2.OKChannel = schan1.OKChannel
	schan2.QUITChannel = schan1.QUITChannel
	schan2.QUERYChannel = schan1.QUERYChannel
	return schan1, schan2
}

// An alternative to calling the function newSessionChannelPair
// is to do schan2 := schan1.dual()
func (schan1 *SessionChannel) Dual() *SessionChannel {
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	schan2.in = schan1.out
	schan2.out = schan1.in
	schan2.OKChannel = schan1.OKChannel
	schan2.QUITChannel = schan1.QUITChannel
	schan2.QUERYChannel = schan1.QUERYChannel
	return schan2
}

// Sends

func (schan *SessionChannel) SendInt(n int) {
	schan.out.IntChannel <- n
}

func (schan *SessionChannel) SendString(str string) {
	schan.out.StringChannel <- str
}

func (schan *SessionChannel) SendAddress(addr Address) {
	schan.out.AddressChannel <- addr
}

func (schan *SessionChannel) SendDate(date Date) {
	schan.out.DateChannel <- date
}

func (schan *SessionChannel) SendAccept(acc Accept) {
	schan.out.AcceptChannel <- acc
}

func (schan *SessionChannel) SendReject(rej Reject) {
	schan.out.RejectChannel <- rej
}

// Receives

func (schan *SessionChannel) ReceiveInt() int {
	return <-schan.in.IntChannel
}

func (schan *SessionChannel) ReceiveString() string {
	return <-schan.in.StringChannel
}

func (schan *SessionChannel) ReceiveAddress() Address {
	return <-schan.in.AddressChannel
}

func (schan *SessionChannel) ReceiveDate() Date {
	return <-schan.in.DateChannel
}

func (schan *SessionChannel) ReceiveAccept() Accept {
	return <-schan.in.AcceptChannel
}

func (schan *SessionChannel) ReceiveReject() Reject {
	return <-schan.in.RejectChannel
}
