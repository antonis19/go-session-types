----------------- GMC Check -----------------
Parsing CFSMs file... (factor 0)
Machine 49: True
Machine 4: False
Machine 48: True
Machine 1: False
Machine 46: True
Machine 7: False
Machine 47: True
Machine 8: False
Machine 2: False
Machine 5: False
Machine 21: False
Machine 29: False
Machine 42: False
Machine 33: False
Machine 19: False
Machine 37: False
Machine 11: False
Machine 45: False
Machine 41: False
Machine 15: False
Machine 0: False
Machine 25: False
Machine 28: False
Machine 17: False
Machine 9: False
Machine 44: False
Machine 30: False
Machine 26: False
Machine 34: False
Machine 18: False
Machine 24: False
Machine 3: False
Machine 40: False
Machine 20: False
Machine 14: False
Machine 22: False
Machine 38: False
Machine 10: False
Machine 27: False
Machine 23: False
Machine 13: False
Machine 39: False
Machine 43: False
Machine 31: False
Machine 35: False
Machine 12: False
Machine 36: False
Machine 16: False
Machine 6: False
Machine 32: False

----------------- Global Graph Synthesis -----------------
Parsing PN file...
Transformation 1 (One-Source Petri Net)
Transformation 2 (Joined Petri Net)
Transformation 3 (PN to Pre-Global Graph)
Transformation 4 (Pre-Global Graph to Global Graph)
Global Graph Construction Done; see output folder.
Current directory is: /home/user/Desktop/go-session-types/gosessiontypes/travelingagency/asynchronous/third_party/gmc-synthesis/inputs
arg1 = /home/user/Desktop/go-session-types/gosessiontypes/travelingagency/asynchronous/third_party/gmc-synthesis/inputs/correct_cfsms

Execution time:  564.851644993   ( Start: 1528827885.51  --> End: 1528828450.36 )
