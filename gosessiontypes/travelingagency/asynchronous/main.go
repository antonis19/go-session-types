package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

import . "./schan"

type end struct{}

func client(schanAgency *SessionChannel, schanService *SessionChannel, endChannel chan end) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("[Client] Enter journey name , ACCEPT to accept quote, or REJECT to reject quote : ")
	scanner.Scan()
	text := scanner.Text()
	if text == "ACCEPT" {
		schanAgency.OKChannel <- OK{}
		schanAgency.SendAccept(Accept{})
		schanService.SendAddress(Address{"Main Street", 4})
		date := schanService.ReceiveDate()
		fmt.Println("[Client] Received date ", date)
		endChannel <- end{}
	} else if text == "REJECT" {
		schanAgency.QUITChannel <- QUIT{}
		schanAgency.SendReject(Reject{})
		endChannel <- end{}
	} else {
		schanAgency.QUERYChannel <- QUERY{}
		schanAgency.SendString(text)
		price := schanAgency.ReceiveInt()
		fmt.Println("[Client] Price for journey is :", price)
		client(schanAgency, schanService, endChannel)
	}
}

func agency(schanClient *SessionChannel, schanService *SessionChannel, endChannel chan end) {
	// prices for select journeys
	var prices = map[string]int{"Madrid": 400, "Mallorca": 380, "London": 499}
	// information on the journeys
	var infos = map[string]string{"Madrid": "The trip lasts 3 days and includes...", "Mallorca": "14 days at the beach...", "London": "One weekend with visits including the House of Parliaments..."}
	select {
	case <-schanClient.OKChannel:
		schanClient.ReceiveAccept()
		schanService.OKChannel <- OK{}
		schanService.SendAccept(Accept{})
		endChannel <- end{}
	case <-schanClient.QUITChannel:
		schanClient.ReceiveReject()
		schanService.QUITChannel <- QUIT{}
		schanService.SendReject(Reject{})
		endChannel <- end{}
	case <-schanClient.QUERYChannel:
		journey := schanClient.ReceiveString()
		price := prices[journey]
		schanClient.SendInt(price)
		info := infos[journey]
		schanService.QUERYChannel <- QUERY{}
		schanService.SendString(info)
		agency(schanClient, schanService, endChannel)
	}
}

func service(schanClient *SessionChannel, schanAgency *SessionChannel, endChannel chan end) {
	select {
	case <-schanAgency.OKChannel:
		schanAgency.ReceiveAccept()
		address := schanClient.ReceiveAddress()
		fmt.Println("[Service] Address of the client is ", address)
		schanClient.SendDate(Date{2018, time.June, 26})
		endChannel <- end{}
	case <-schanAgency.QUITChannel:
		schanAgency.ReceiveReject()
		endChannel <- end{}
	case <-schanAgency.QUERYChannel:
		info := schanAgency.ReceiveString()
		fmt.Println("[Service] info is ", info)
		service(schanClient, schanAgency, endChannel)
	}
}

func main() {
	bufsize := 4 // choose an arbitrary buffer size
	schanClientAgency1, schanClientAgency2 := NewSessionChannelPair(bufsize)
	schanClientService1, schanClientService2 := NewSessionChannelPair(bufsize)
	schanAgencyService1, schanAgencyService2 := NewSessionChannelPair(bufsize)
	endChannel := make(chan end)
	// Run goroutines
	go client(schanClientAgency1, schanClientService1, endChannel)
	go agency(schanClientAgency2, schanAgencyService1, endChannel)
	go service(schanClientService2, schanAgencyService2, endChannel)
	// Wait for parties to end
	<-endChannel
	<-endChannel
	<-endChannel
}
