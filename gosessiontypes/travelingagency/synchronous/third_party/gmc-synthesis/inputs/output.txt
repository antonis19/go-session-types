----------------- GMC Check -----------------
Parsing CFSMs file... (factor 0)
Machine 5: False
Machine 0: False
Machine 6: False
Machine 15: False
Machine 4: False
Machine 11: False
Machine 2: False
Machine 8: False
Machine 19: True
Machine 1: False
Machine 3: False
Machine 20: True
Machine 7: False
Machine 10: False
Machine 22: True
Machine 21: False
Machine 12: False
Machine 14: False
Machine 9: False
Machine 13: False
Machine 16: False
Machine 17: False
Machine 18: False

----------------- Global Graph Synthesis -----------------
Parsing PN file...
Transformation 1 (One-Source Petri Net)
Transformation 2 (Joined Petri Net)
Transformation 3 (PN to Pre-Global Graph)
Transformation 4 (Pre-Global Graph to Global Graph)
Global Graph Construction Done; see output folder.
Current directory is: /home/user/Desktop/go-session-types/gosessiontypes/travelingagency/synchronous/third_party/gmc-synthesis/inputs
arg1 = /home/user/Desktop/go-session-types/gosessiontypes/travelingagency/synchronous/third_party/gmc-synthesis/inputs/output_cfsms

Execution time:  1.80716180801   ( Start: 1528809930.46  --> End: 1528809932.27 )
