package schan

import (
	"time"
)

type Address struct {
	StreetName   string
	StreetNumber int
}
type Date struct {
	Year  int
	Month time.Month
	Day   int
}

type Accept struct{}
type Reject struct{}

// SessionChannel contains channels for all datatypes that will be exchanged
// throughout the session
type SessionChannel struct {
	IntChannel     chan int
	StringChannel  chan string
	AddressChannel chan Address
	DateChannel    chan Date
	AcceptChannel  chan Accept
	RejectChannel  chan Reject
}

func (schan *SessionChannel) Make() {
	schan.IntChannel = make(chan int)
	schan.StringChannel = make(chan string)
	schan.AddressChannel = make(chan Address)
	schan.DateChannel = make(chan Date)
	schan.AcceptChannel = make(chan Accept)
	schan.RejectChannel = make(chan Reject)
}
