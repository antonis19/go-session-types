package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

import . "./schan"

type end struct{}

func client(schanAgency *SessionChannel, schanService *SessionChannel, endChannel chan end) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("[Client] Enter journey name , ACCEPT to accept quote, or REJECT to reject quote : ")
	scanner.Scan()
	text := scanner.Text()
	if text == "ACCEPT" {
		schanAgency.AcceptChannel <- Accept{}
		schanService.AddressChannel <- Address{"Main Street", 4}
		date := <-schanService.DateChannel
		fmt.Println("[Client] Received date ", date)
		endChannel <- end{}
	} else if text == "REJECT" {
		schanAgency.RejectChannel <- Reject{}
		endChannel <- end{}
	} else {
		schanAgency.StringChannel <- text
		price := <-schanAgency.IntChannel
		fmt.Println("[Client] Price for journey is :", price)
		client(schanAgency, schanService, endChannel)
	}
}

func agency(schanClient *SessionChannel, schanService *SessionChannel, endChannel chan end) {
	// prices for select journeys
	var prices = map[string]int{"Madrid": 400, "Mallorca": 380, "London": 499}
	// information on the journeys
	var infos = map[string]string{"Madrid": "The trip lasts 3 days and includes...", "Mallorca": "14 days at the beach...", "London": "One weekend with visits including the House of Parliaments..."}
	select {
	case <-schanClient.AcceptChannel:
		schanService.AcceptChannel <- Accept{}
		endChannel <- end{}
	case <-schanClient.RejectChannel:
		schanService.RejectChannel <- Reject{}
		endChannel <- end{}
	case journey := <-schanClient.StringChannel:
		price := prices[journey]
		schanClient.IntChannel <- price
		info := infos[journey]
		schanService.StringChannel <- info
		agency(schanClient, schanService, endChannel)
	}
}

func service(schanClient *SessionChannel, schanAgency *SessionChannel, endChannel chan end) {
	select {
	case <-schanAgency.AcceptChannel:
		address := <-schanClient.AddressChannel
		fmt.Println("[Service] Address of the client is ", address)
		schanClient.DateChannel <- Date{2018, time.June, 26}
		endChannel <- end{}
	case <-schanAgency.RejectChannel:
		endChannel <- end{}
	case info := <-schanAgency.StringChannel:
		fmt.Println("[Service] info is ", info)
		service(schanClient, schanAgency, endChannel)
	}
}

func main() {
	schanClientAgency, schanClientService, schanAgencyService := new(SessionChannel), new(SessionChannel), new(SessionChannel)
	schanClientAgency.Make()
	schanClientService.Make()
	schanAgencyService.Make()
	endChannel := make(chan end)
	// Run goroutines
	go client(schanClientAgency, schanClientService, endChannel)
	go agency(schanClientAgency, schanAgencyService, endChannel)
	go service(schanClientService, schanAgencyService, endChannel)
	// Wait for parties to end
	<-endChannel
	<-endChannel
	<-endChannel
}
