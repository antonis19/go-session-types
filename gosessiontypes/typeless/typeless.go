// Write Go code here
package main

import "fmt"

func main() {
    ch := make(chan interface{})   // Create channel ch
    go func(ch chan interface{}) { // Spawn goroutine
        ch <- 42          // Send value to ch
    }(ch)
	var val string = (<-ch).(string)
    fmt.Println(val)      // Recv value from ch
}
