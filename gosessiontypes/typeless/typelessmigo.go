// Write Go code here
package main

import "fmt"

func main() {
    ch := make(chan interface{})   // Create channel ch
    go func(ch chan interface{}) { // Spawn goroutine
        ch <- "hi"          // Send value to ch
    }(ch)
    var val int = (<-ch).(int)
    fmt.Println(val)      // Recv value from ch
}
