package main

// session type : !int.?string.!int.end
func clientRoutine(out chan<- interface{}, in <-chan interface{}) {
	out <- 42
	// Loop over all input from the server...
	input := (<-in).(string)
	println("Received from server: ", input)
	// ...and send the results back.
	out <- 43
	close(out)
}

// session type : ?int.!string.?int.end
func serverRoutine(out chan<- interface{}, in <-chan interface{}) {
	val := (<-in).(int)
	println("Received from client: ", val)
	// send strings to be capitalized to out
	out <- "hi"
	// don't forget to close the channel! this is how all connected
	// clients know that there's no more work coming.
	println((<-in).(int))
	close(out)
}

func main() {
	out, in := make(chan interface{}), make(chan interface{})
	go clientRoutine(out, in)
	serverRoutine(in, out)
}
