package main

import (
	"log"
	"net"

	"github.com/dradtke/distchan"
)

func makeServerConnection() net.Listener {
	ln, err := net.Listen("tcp", "localhost:5678")
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return ln
}

func serverRoutine(out chan<- interface{}, in <-chan interface{}) {
	val := (<-in).(int)
	println("Received from client: ", val)
	// send strings to be capitalized to out
	out <- "hi"
	// don't forget to close the channel! this is how all connected
	// clients know that there's no more work coming.
	println((<-in).(int))
	location := (<-in).(Location)
	println("Received Location ", location.x, location.y)
	close(out)
}

func main() {
	out, in := make(chan interface{}), make(chan interface{})
	ln := makeServerConnection()
	server, _ := distchan.NewServer(ln, out, in)
	server.Start()
	server.WaitUntilReady() // wait until we have at least one worker available
	serverRoutine(out, in)
}
