package main

import (
	"log"
	"net"

	"github.com/dradtke/distchan"
)

func makeConnection() net.Conn {
	conn, err := net.Dial("tcp", "localhost:5678") // must be able to connect to the server
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return conn
}

func clientRoutine(out chan<- interface{}, in <-chan interface{}) {
	out <- 42
	// Loop over all input from the server...
	input := (<-in).(string)
	println("Received from server: ", input)
	// ...and send the results back.
	out <- "hi"
	location := Location{
		x: 200,
		y: 201,
	}
	out <- location
	close(out)
}

func main() {
	out, in := make(chan interface{}), make(chan interface{})
	conn := makeConnection()
	client, _ := distchan.NewClient(conn, out, in)
	client.Start()
	clientRoutine(out, in)
	<-client.Done()
}
