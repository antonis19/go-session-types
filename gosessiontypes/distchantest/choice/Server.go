package main

import (
	"fmt"
	"log"
	"net"
	"time"

	"github.com/dradtke/distchan"
)

func makeServerConnection() net.Listener {
	ln, err := net.Listen("tcp", "localhost:5678")
	if err != nil {
		log.Fatalln(err)
	}
	return ln
}

func serverRoutine(out chan<- int, in <-chan int, endChannel chan<- bool) {
	select {
	case val := <-in:
		fmt.Println("[Server] ?int ", val)
	case out <- 56:
		fmt.Println("[Server] !int")
	}
	time.Sleep(4 * time.Second)
	endChannel <- true
}

func main() {
	out, in := make(chan int), make(chan int)
	endChannel := make(chan bool)
	ln := makeServerConnection()
	server, _ := distchan.NewServer(ln, out, in)
	server.Start()
	server.WaitUntilReady() // wait until we have at least one worker available
	go serverRoutine(out, in, endChannel)
	<-endChannel // wait for goroutine to finish
	//close(out) // close out channel so that client can be done
}
