package main

import (
	"fmt"
	"log"
	"net"

	"github.com/dradtke/distchan"
)

func makeConnection() net.Conn {
	conn, err := net.Dial("tcp", "localhost:5678") // must be able to connect to the server
	if err != nil {
		log.Fatalln(err)
	}
	return conn
}

var x = 20

func clientRoutine(out chan<- int, in <-chan int, endChannel chan<- bool) {
	if x < 42 {
		out <- 42
		fmt.Println("[Client] !int")
	} else {
		val := <-in
		fmt.Println("[Client] ?int  ", val)
	}
	endChannel <- true
}

func main() {
	out, in := make(chan int), make(chan int)
	endChannel := make(chan bool)
	conn := makeConnection()
	client, _ := distchan.NewClient(conn, out, in)
	client.Start()
	go clientRoutine(out, in, endChannel)
	<-endChannel // wait for goroutine to finish
}
