package main

import (
	"log"
	"net"

	"github.com/dradtke/distchan"
)

func makeConnection() net.Conn {
	conn, err := net.Dial("tcp", "localhost:5678") // must be able to connect to the server
	if err != nil {
		log.Fatalln(err)
	}
	return conn
}

func clientRoutine(out chan int, in chan int, endChannel chan<- bool) {
	// !int
	out <- 42
	// ?int
	input := <-in
	println("Received from server: ", input)
	// println("before receive")
	// val := <-out
	// println("val is ",val)
	endChannel <- true
}

func main() {
	out, in := make(chan int), make(chan int)
	endChannel := make(chan bool)
	conn := makeConnection()
	client, _ := distchan.NewClient(conn, out, in)
	client.Start()
	go clientRoutine(out, in, endChannel)
	<-endChannel // wait for goroutine to finish
}
