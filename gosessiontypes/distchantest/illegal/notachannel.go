package main

import (
	"log"
	"net"

	"github.com/dradtke/distchan"
)

func main() {
	conn, _ := net.Dial("tcp", "localhost:5678")
	out, in := make([]int, 5), make([]int, 5)
	client, err := distchan.NewClient(conn, out, in)
	if err != nil {
		log.Fatalln(err)
	}
	client.Start()
}
