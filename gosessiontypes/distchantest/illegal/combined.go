package main

func serverRoutine(out chan<- int, in <-chan int, endChannel chan <- bool) {
	// ?int
	val := <-in
	println("Received from client: ", val)
	// !int
	out <- val
	//time.Sleep(1*time.Second)
	endChannel <- true
}

func clientRoutine(out chan<- int, in <-chan int, endChannel chan <-bool) {
	// !int
	out <- 42
	// ?int 
	input := <-in
	println("Received from server: ", input)
	endChannel <- true
}

func main() {
	out, in := make(chan int), make(chan int)
	endChannel := make(chan bool)
	go clientRoutine(out, in, endChannel)
	go serverRoutine(in, out, endChannel)
	<-endChannel
	<-endChannel
}
