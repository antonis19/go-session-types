package main

const BUFFERSIZE = 10

type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
}

func makeSessionChannel() SessionChannel {
	return SessionChannel {
		boolChannel : make(chan bool,BUFFERSIZE),
		stringChannel : make(chan string, BUFFERSIZE),
		intChannel : make(chan int, BUFFERSIZE),
	}
}

// !bool . !int . ?int . ?string. end
func party1(in SessionChannel, out SessionChannel, end chan <- bool) {
	// !bool
	out.boolChannel <- true
	// !int
	out.intChannel <- 42
	// ?int
	recvdInt := <-in.intChannel
	println("Party 1 received: ",recvdInt)
	// ?string
	recvdString := <-in.stringChannel
	println("Party 1 received: ",recvdString)
	// end
	end<-true
}

// ?bool . ?int . !int . !string . end
func party2(in SessionChannel, out SessionChannel, end chan <- bool) {
	// ?bool
	recvdBool := <-in.boolChannel
	println("Party 2  received: ",recvdBool)
	// ?int
	recvdInt := <-in.intChannel
	println("Party 2 received: ",recvdInt)
	// !int
	out.intChannel <- 200
	// ! string
	out.stringChannel <- "Hello World"
	end<-true
}

func main() {
	in := SessionChannel {
		boolChannel : make(chan bool,BUFFERSIZE),
		stringChannel : make(chan string,BUFFERSIZE),
		intChannel : make(chan int,BUFFERSIZE),
	}
	
	out:= SessionChannel {
		boolChannel : make(chan bool,BUFFERSIZE),
		stringChannel : make(chan string,BUFFERSIZE),
		intChannel : make(chan int,BUFFERSIZE),
	}
	
	end := make(chan bool)
	go party1(in,out,end)
	go party2(out,in,end)
	<-end
	<-end
	println("Exiting")
}
