package main


type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
}

func makeSessionChannel() SessionChannel {
	return SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
	}
}

// !bool . !int . ?int . ?string. end
func party1(sessionChannel SessionChannel, end chan <- bool) {
	// !bool
	sessionChannel.boolChannel <- true
	// !int
	sessionChannel.intChannel <- 42
	// ?int
	recvdInt := <-sessionChannel.intChannel
	println("Party 1 received: ",recvdInt)
	// ?string
	recvdString := <-sessionChannel.stringChannel
	println("Party 1 received: ",recvdString)
	// end
	end<-true
}

// ?bool . ?int . !int . !string . end
func party2(sessionChannel SessionChannel, end chan <- bool) {
	// ?bool
	recvdBool := <-sessionChannel.boolChannel
	println("Party 2  received: ",recvdBool)
	// ?int
	recvdInt := <-sessionChannel.intChannel
	println("Party 2 received: ",recvdInt)
	// !int
	sessionChannel.intChannel <- 200
	// ! string
	sessionChannel.stringChannel <- "Hello World"
	end<-true
}

func main() {
	sessionChannel := SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
	}
	end := make(chan bool)
	go party1(sessionChannel,end)
	go party2(sessionChannel,end)
	<-end
	<-end
	println("Exiting")
}
