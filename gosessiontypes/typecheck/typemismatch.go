package main

type number int

func Sender(ch chan number) {
	ch <- int(42)
}

func Receiver(ch chan number) {
	<-ch
}

func main() {
	ch := make(chan number)
	go Sender(ch)
	Receiver(ch)
}
