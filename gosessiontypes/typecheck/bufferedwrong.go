package main

var BUFFERSIZE = 10

type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
}

func makeSessionChannel() SessionChannel {
	return SessionChannel {
		boolChannel : make(chan bool,BUFFERSIZE),
		stringChannel : make(chan string, BUFFERSIZE),
		intChannel : make(chan int, BUFFERSIZE),
	}
}

// !bool . !int . ?int . ?string. end
func party1(sessionChannel SessionChannel, end chan <- bool) {
	println("Party 1 started")
	// !bool
	sessionChannel.boolChannel <- true
	println("Part 1 sent a bool")
	// !int
	sessionChannel.intChannel <- 42
	println("Party 1 came here")
	// ?int
	recvdInt := <-sessionChannel.intChannel
	println("Party 1 received: ",recvdInt)
	// ?string
	recvdString := <-sessionChannel.stringChannel
	println("Party 1 received: ",recvdString)
	// end
	end<-true
}

// ?int . ?bool . !int . !string . end
func party2(sessionChannel SessionChannel, end chan <- bool) {
	// ?int
	recvdInt := <-sessionChannel.intChannel
	println("Party 2 received: ",recvdInt)
	// ?bool
	recvdBool := <-sessionChannel.boolChannel
	println("Party 2  received: ",recvdBool)
	// !int
	sessionChannel.intChannel <- 200
	// ! string
	sessionChannel.stringChannel <- "Hello World"
	end<-true
}

func main() {
	sessionChannel := SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
	}
	end := make(chan bool)
	go party1(sessionChannel,end)
	go party2(sessionChannel,end)
	<-end
	<-end
	println("Exiting")
}
