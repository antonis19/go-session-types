package main

import (
	"log"
	"net"
	"strings"

	"github.com/dradtke/distchan"
)

func makeConnection() net.Conn {
	conn, err := net.Dial("tcp", "localhost:5678") // must be able to connect to the server
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return conn
}

func clientRoutine(out chan<- string, in <-chan string) {
	out <- "hi"
	// Loop over all input from the server...
	input := <-in
	println("Received from server: ", input)
	capitalized := strings.ToUpper(input)
	// ...and send the results back.
	out <- capitalized
	close(out)
}

func main() {
	out, in := make(chan string), make(chan string)
	conn := makeConnection()
	client, _ := distchan.NewClient(conn, out, in)
	client.Start()
	clientRoutine(out, in)
	<-client.Done()
}
