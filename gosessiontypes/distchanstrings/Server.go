package main

import (
	"log"
	"net"

	"github.com/dradtke/distchan"
)

func makeServerConnection() net.Listener {
	ln, err := net.Listen("tcp", "localhost:5678")
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return ln
}

func serverRoutine(out chan<- string, in <-chan string) {
	val := <-in
	println("Received from client: ", val)
	// send strings to be capitalized to out
	out <- val
	// don't forget to close the channel! this is how all connected
	// clients know that there's no more work coming.
	println(<-in)
	close(out)
}

func main() {
	ln := makeServerConnection()
	out, in := make(chan string), make(chan string)
	server, _ := distchan.NewServer(ln, out, in)
	server.Start()
	server.WaitUntilReady() // wait until we have at least one worker available
	serverRoutine(out, in)
}
