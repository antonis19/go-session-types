package main

type Label struct{}

// The two labels used in the protocol
type OK Label
type QUIT Label

type end struct{}

// Channels is a struct encapsulating the channels for datatypes that will be exchanged
type Channels struct {
	stringChannel chan string
	intChannel    chan int
	boolChannel   chan bool
}

// SessionChannel
type SessionChannel struct {
	okChannel   chan OK
	quitChannel chan QUIT
	in          *Channels
	out         *Channels
}

func (channels *Channels) make(bufsize int) {
	channels.stringChannel = make(chan string, bufsize)
	channels.intChannel = make(chan int, bufsize)
	channels.boolChannel = make(chan bool, bufsize)
}

func (schan *SessionChannel) make(bufsize int) {
	schan.in = new(Channels)
	schan.out = new(Channels)
	schan.in.make(bufsize)
	schan.out.make(bufsize)
	// make label channels
	schan.okChannel = make(chan OK)
	schan.quitChannel = make(chan QUIT)
}

// This function returns a pair of SessionChannels that share the same
// underlying channels, just with different names
func newSessionChannelPair(bufsize int) (*SessionChannel, *SessionChannel) {
	schan1 := new(SessionChannel)
	schan1.make(bufsize)
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	// schan1 and schan2 share the same channels, just with different names
	schan2.in = schan1.out
	schan2.out = schan1.in
	// these channels are for sending/receiving labels
	schan2.okChannel = schan1.okChannel
	schan2.quitChannel = schan1.quitChannel
	return schan1, schan2
}

// An alternative to calling the function newSessionChannelPair
// is to do schan2 := schan1.dual()
func (schan1 *SessionChannel) dual() *SessionChannel {
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	schan2.in = schan1.out
	schan2.out = schan1.in
	schan2.okChannel = schan1.okChannel
	schan2.quitChannel = schan1.quitChannel
	return schan2
}

// Send/Receive operations on datatypes hide implementation details
// We cannot do the same thing for labels because select-case requires explicit
// channel operations on the cases

// Sends

func (schan *SessionChannel) sendString(str string) {
	schan.out.stringChannel <- str
}

func (schan *SessionChannel) sendInt(n int) {
	schan.out.intChannel <- n
}

func (schan *SessionChannel) sendBool(b bool) {
	schan.out.boolChannel <- b
}

//Receives

func (schan *SessionChannel) receiveString() string {
	return <-schan.in.stringChannel
}

func (schan *SessionChannel) receiveInt() int {
	return <-schan.in.intChannel
}

func (schan *SessionChannel) receiveBool() bool {
	return <-schan.in.boolChannel
}

var x = 299

func externalChoice(schan *SessionChannel) {
	select {
	case <-schan.okChannel: // OK branch was selected
		schan.receiveInt()
	case <-schan.quitChannel: // QUIT branch was selected
		schan.sendString("Hello World")
	}
}

func main() {
	bufsize := 4
	schan, _ := newSessionChannelPair(bufsize)
	externalChoice(schan)
}
