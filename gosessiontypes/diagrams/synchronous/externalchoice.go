package main

import "fmt"


type SessionChannel struct {
	stringChannel chan string
	intChannel    chan int
	boolChannel   chan bool
}

func (schan *SessionChannel) make() {
	schan.stringChannel = make(chan string)
	schan.intChannel = make(chan int)
	schan.boolChannel = make(chan bool)
}

func externalChoice(schan *SessionChannel) {
	select {
	case <-schan.intChannel: // ?int
		fmt.Println("Received an int")
	case schan.stringChannel <- "Hello World": // !string
		fmt.Println("Sent a string")
	case <-schan.boolChannel: // ?bool
		fmt.Println("Received a bool")
	}
}

func main() {
	schan := new(SessionChannel)
	schan.make()
	externalChoice(schan)
}
