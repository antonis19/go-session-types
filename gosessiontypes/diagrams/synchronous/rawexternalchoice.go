package main

import "fmt"

func externalChoice(intChannel chan int, stringChannel chan string, boolChannel chan bool) {
	select {
	case <-intChannel: // ?int
		fmt.Println("Received an int")
	case stringChannel <- "Hello World": // !string
		fmt.Println("Sent a string")
	case <-boolChannel: // ?bool
		fmt.Println("Received a bool")
	}
}

func main() {
	intChannel, stringChannel, boolChannel := make(chan int), make(chan string), make(chan bool)
	externalChoice(intChannel, stringChannel, boolChannel)

}
