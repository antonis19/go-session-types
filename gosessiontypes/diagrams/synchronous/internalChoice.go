package main

var x = 299

type SessionChannel struct {
	stringChannel chan string
	intChannel    chan int
	boolChannel   chan bool
}

func (schan *SessionChannel) make() {
	schan.stringChannel = make(chan string)
	schan.intChannel = make(chan int)
	schan.boolChannel = make(chan bool)
}

func internalChoice(schan *SessionChannel) {
	if x < 42 {
		// !int
		schan.intChannel <- 99
	} else if x > 42 {
		// ?string
		<-schan.stringChannel
	} else {
		// !bool
		schan.boolChannel <- true
	}
}

func main() {
	schan := new(SessionChannel)
	schan.make()
	internalChoice(schan)

}
