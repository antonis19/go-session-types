package main

var x = 299

func internalChoice(intChannel chan int, stringChannel chan string, boolChannel chan bool) {
	if x < 42 {
		// !int
		intChannel <- 99
	} else if x > 42 {
		// ?string
		<-stringChannel
	} else {
		// !bool
		boolChannel <- true
	}
}

func main() {
	intChannel, stringChannel, boolChannel := make(chan int), make(chan string), make(chan bool)
	internalChoice(intChannel, stringChannel, boolChannel)

}
