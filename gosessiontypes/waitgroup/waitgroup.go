package main

import (
	"sync"
)

type SessionChannel struct {
	intChannel    chan int
	boolChannel   chan bool
	stringChannel chan string
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel {
		intChannel : make(chan int),
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
	}
}

var x = 299


func internalChoice(schan *SessionChannel, waitgroup *sync.WaitGroup) {
	if x < 42 {
		// !int
		schan.intChannel <- 99 
	} else if x > 42 {
		// ?string
		<-schan.stringChannel
	} else {
		// !bool
		schan.boolChannel <- true
	}
	waitgroup.Done()
}


func externalChoice(schan *SessionChannel, waitgroup *sync.WaitGroup) {
	select {
		case <-schan.intChannel : // ?int
			println("Received an int")
		case schan.stringChannel <- "Hello World" : // !string
			println("Sent a string")
		case <-schan.boolChannel : // ?bool
			println("Received a bool")
	}
	waitgroup.Done()
	
}


func main() {
	var waitgroup sync.WaitGroup
	schan := makeSessionChannel()
	waitgroup.Add(1)
	go externalChoice(schan,&waitgroup)
	waitgroup.Add(1)
	go internalChoice(schan,&waitgroup)
	waitgroup.Wait()
}
