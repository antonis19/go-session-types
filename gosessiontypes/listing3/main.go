package main
import (
  "fmt"
  "time"
  "net"
)

func Send(ch chan<- int) {
	ch <- 42
}

func Recv(ch <- chan int, done chan<- int) {
	val:= <-ch
	done<-val
}


func Work() {
	for {
		fmt.Println("Working...")
		time.Sleep(1*time.Second)
	}
}


func main() {
	ch, done := make(chan int), make(chan int)
	
	
	conn, err := net.Dial("tcp", "localhost:5678")
	if err != nil {
		fmt.Println(err)
	}
	_ = conn // this is to prevent unused variable error
	
	go Send(ch)
	go Recv(ch,done)
	go Recv(ch,done)
	go Work()
	
	<-done
	<-done
	
}
