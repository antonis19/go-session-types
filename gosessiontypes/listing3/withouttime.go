package main

func Send(ch chan<- int) {
	ch <- 42
}

func Recv(ch <- chan int, done chan<- int) {
	val:= <-ch
	done<-val
}


func main() {
	ch, done := make(chan int), make(chan int)
	
	go Send(ch)
	go Recv(ch,done)
	go Recv(ch,done)	
	<-done
	<-done
	
}
