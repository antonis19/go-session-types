package main
import (
  "fmt"
  "time"
)



func Send(ch chan<- int) {
	ch <- 42
}

func Recv(ch <- chan int, done chan<- int) {
	val:= <-ch
	done<-val
}


func Work() {
	for {
		fmt.Println("Working...")
		time.Sleep(1*time.Second)
	}
}


func main() {
	ch, done := make(chan int), make(chan int)
	
	go Send(ch)
	go Recv(ch,done)
	go Recv(ch,done)
	go Work()
	
	<-done
	<-done
	
}
