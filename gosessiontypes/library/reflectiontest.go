package main

import (
	"fmt"
	"milton19/distchan"
)

type Address string
type Date string
type Bye struct{}

type Label string

type LabelChannel chan Label
type AddressChannel chan Address
type DateChannel chan Date
type ByeChannel chan Bye

type Channel interface {
	Send(i interface{})
	Receive() interface{}
}

type Channels struct {
	labelChannel   LabelChannel
	addressChannel AddressChannel
	dateChannel    DateChannel
	byeChannel     ByeChannel
}

type SessionChannel struct {
	in  *Channels
	out *Channels
}

func (schan *SessionChannel) GetInputChannels() []interface{} {
	return []interface{}{schan.in.labelChannel, schan.in.addressChannel,
		schan.in.dateChannel, schan.in.byeChannel}
}

func (schan *SessionChannel) GetOutputChannels() []interface{} {
	return []interface{}{schan.out.labelChannel, schan.out.addressChannel,
		schan.out.dateChannel, schan.out.byeChannel}
}

func (channels *Channels) make(bufsize int) {
	channels.labelChannel = make(chan Label, bufsize)
	channels.addressChannel = make(chan Address, bufsize)
	channels.dateChannel = make(chan Date, bufsize)
	channels.byeChannel = make(chan Bye, bufsize)
}

func (schan *SessionChannel) make(bufsize int) {
	schan.in = new(Channels)
	schan.out = new(Channels)
	schan.in.make(bufsize)
	schan.out.make(bufsize)
}

func newSessionChannelPair(bufsize int) (*SessionChannel, *SessionChannel) {
	schan1 := new(SessionChannel)
	schan1.make(bufsize)
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	schan2.in = schan1.out
	schan2.out = schan1.in
	return schan1, schan2
}

// Channel methods implementation

func (ch LabelChannel) Send(label Label) {
	ch <- label
}

func (ch LabelChannel) Receive() Label {
	return <-ch
}

func (ch AddressChannel) Send(addr Address) {
	ch <- addr
}

func (ch AddressChannel) Receive() Address {
	return <-ch
}

//Date
func (ch DateChannel) Send(date Date) {
	ch <- date
}

func (ch DateChannel) Receive() Date {
	return <-ch
}

// Bye
func (ch ByeChannel) Send(bye Bye) {
	ch <- bye
}

func (ch ByeChannel) Receive() Bye {
	return <-ch
}

func (schan1 *SessionChannel) dual() *SessionChannel {
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	schan2.in = schan1.out
	schan2.out = schan1.in
	return schan2
}

// SessionChannel methods implementation

// Sends

func (schan *SessionChannel) sendLabel(label Label) {
	schan.out.labelChannel.Send(label)
}

func (schan *SessionChannel) sendAddress(address Address) {
	//schan.addressChannel <- address
	schan.out.addressChannel.Send(address)
}

func (schan *SessionChannel) sendDate(date Date) {
	//schan.dateChannel <- date
	schan.out.dateChannel.Send(date)
}

func (schan *SessionChannel) sendBye(bye Bye) {
	schan.out.byeChannel.Send(bye)
}

// Receives

func (schan *SessionChannel) receiveLabel() Label {
	return schan.in.labelChannel.Receive()
}

func (schan *SessionChannel) receiveAddress() Address {
	//return <-schan.addressChannel
	return schan.in.addressChannel.Receive()
}

func (schan *SessionChannel) receiveDate() Date {
	return schan.in.dateChannel.Receive()
}

func (schan *SessionChannel) receiveBye() Bye {
	return schan.in.byeChannel.Receive()
}

func Sender(schan *SessionChannel, endChannel chan bool) {
	funds, quote, buyer1quote := 30, 50, 25
	if quote-buyer1quote <= funds {
		schan.sendLabel("checkout")
		schan.sendAddress("Main Street")
		date := schan.receiveDate()
		fmt.Println("date is ", date)
		endChannel <- true
	} else {
		schan.sendLabel("quit")
		schan.sendBye(Bye{})
		endChannel <- true
	}
}

func Receiver(schan *SessionChannel, endChannel chan bool) {
	var label Label = schan.receiveLabel()
	if label == "checkout" {
		address := schan.receiveAddress()
		fmt.Println("address is ", address)
		schan.sendDate("2018-01-20")
		endChannel <- true
	} else if label == "quit" {
		schan.receiveBye()
		fmt.Println("Received bye message ")
		endChannel <- true
	} else {
		panic("Unexpected choice")
	}
}

func main() {
	schan1 := new(SessionChannel)
	schan1.make(0)
	distchan.Check(schan1)

}
