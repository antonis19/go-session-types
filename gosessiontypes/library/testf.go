package main


func send(chint chan <- int, chstring chan <- string, val interface{}) {
	switch  val.(type) {
		case int:
			chint <- val.(int)
		case string:
			chstring <- val.(string)
	}
}

func Sender(chint chan <- int, chstring chan <- string) {
	send(chint,chstring,"hi")
	send(chint,chstring,42)
}

func Receiver(chint <- chan int, chstring <- chan string) {
	select {
		case <-chint :
			println("received an int")
		case <-chstring:
			println("received a string")
		
	}
	
	select {
		case <-chint :
			println("received an int")
		case <-chstring:
			println("received a string")
		
	}

}

func main() {
	chint, chstring := make(chan int), make(chan string)
	go Sender(chint,chstring)
	Receiver(chint,chstring)
	
}
