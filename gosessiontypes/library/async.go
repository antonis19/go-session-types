package main

import "fmt"

// The data types that will be exchanged
type Address string
type Date string
type Bye struct{}

type Label struct{}

// The two labels used in the protocol
type OK Label
type QUIT Label

// Channels is a struct encapsulating the channels for datatypes that will be exchanged
type Channels struct {
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

// SessionChannel
type SessionChannel struct {
	okChannel   chan OK
	quitChannel chan QUIT
	in          *Channels
	out         *Channels
}

func (channels *Channels) make(bufsize int) {
	channels.addressChannel = make(chan Address, bufsize)
	channels.dateChannel = make(chan Date, bufsize)
	channels.byeChannel = make(chan Bye, bufsize)
}

func (schan *SessionChannel) make(bufsize int) {
	schan.in = new(Channels)
	schan.out = new(Channels)
	schan.in.make(bufsize)
	schan.out.make(bufsize)
	// make label channels
	schan.okChannel = make(chan OK, bufsize)
	schan.quitChannel = make(chan QUIT, bufsize)
}

// This function returns a pair of SessionChannels that share the same
// underlying channels, just with different names
func newSessionChannelPair(bufsize int) (*SessionChannel, *SessionChannel) {
	schan1 := new(SessionChannel)
	schan1.make(bufsize)
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	// schan1 and schan2 share the same channels, just with different names
	schan2.in = schan1.out
	schan2.out = schan1.in
	// these channels are for sending/receiving labels
	schan2.okChannel = schan1.okChannel
	schan2.quitChannel = schan1.quitChannel
	return schan1, schan2
}

// An alternative to calling the function newSessionChannelPair
// is to do schan2 := schan1.dual()
func (schan1 *SessionChannel) dual() *SessionChannel {
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	schan2.in = schan1.out
	schan2.out = schan1.in
	schan2.okChannel = schan1.okChannel
	schan2.quitChannel = schan1.quitChannel
	return schan2
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetInputChannels() []interface{} {
	return []interface{}{schan.in.addressChannel, schan.in.dateChannel, schan.in.byeChannel}
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetOutputChannels() []interface{} {
	return []interface{}{schan.out.addressChannel, schan.out.dateChannel, schan.out.byeChannel}
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetLabelChannels() []interface{} {
	return []interface{}{schan.okChannel, schan.quitChannel}
}

// Send/Receive operations on datatypes hide implementation details
// We cannot do the same thing for labels because select-case requires explicit
// channel operations on the cases

// Sends
func (schan *SessionChannel) sendAddress(address Address) {
	schan.out.addressChannel <- address
}

func (schan *SessionChannel) sendDate(date Date) {
	schan.out.dateChannel <- date
}

func (schan *SessionChannel) sendBye(bye Bye) {
	schan.out.byeChannel <- bye
}

//Receives

func (schan *SessionChannel) receiveAddress() Address {
	return <-schan.in.addressChannel
}

func (schan *SessionChannel) receiveDate() Date {
	return <-schan.in.dateChannel
}

func (schan *SessionChannel) receiveBye() Bye {
	return <-schan.in.byeChannel
}

// * {"OK" : !Address . ?Date . end, "QUIT" : !Bye . end}
func Sender(schan *SessionChannel, endChannel chan bool) {
	funds, quote, buyer1quote := 30, 50, 25
	if quote-buyer1quote <= funds {
		schan.okChannel <- OK{}
		schan.sendAddress("Main Street")
		date := schan.receiveDate()
		fmt.Println("date is ", date)
		endChannel <- true
	} else {
		schan.quitChannel <- QUIT{}
		schan.sendBye(Bye{})
		endChannel <- true
	}
}

// & {"OK" : ?Address . !Date . end, "QUIT" : ?Bye . end}
func Receiver(schan *SessionChannel, endChannel chan bool) {
	select {
	case <-schan.okChannel:
		address := schan.receiveAddress()
		fmt.Println("address is ", address)
		schan.sendDate("2018-01-20")
		endChannel <- true
	case <-schan.quitChannel:
		schan.receiveBye()
		fmt.Println("Received bye message ")
		endChannel <- true
	}
}

func main() {
	bufsize := 4 // choose an  arbitary buffer size
	schan1, schan2 := newSessionChannelPair(bufsize)
	endChannel := make(chan bool)
	go Sender(schan1, endChannel)
	go Receiver(schan2, endChannel)
	// Wait for goroutines to finish
	<-endChannel
	<-endChannel
}
