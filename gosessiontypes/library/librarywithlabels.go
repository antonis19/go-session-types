package main

type Address string
type Date string
type Bye struct{}

type Label string

type LabelChannel chan Label
type AddressChannel chan Address
type DateChannel chan Date
type ByeChannel chan Bye

type SessionChannel struct {
	labelChannel   LabelChannel
	addressChannel AddressChannel
	dateChannel    DateChannel
	byeChannel     ByeChannel
}

type Channel interface {
	Send(i interface{})
	Receive() interface{}
}

func (schan *SessionChannel) make() {
	schan.labelChannel = make(chan Label)
	schan.addressChannel = make(chan Address)
	schan.dateChannel = make(chan Date)
	schan.byeChannel = make(chan Bye)
}

// Channel methods implementation

func (ch LabelChannel) Send(label Label) {
	ch <- label
}

func (ch LabelChannel) Receive() Label {
	return <-ch
}

func (ch AddressChannel) Send(addr Address) {
	ch <- addr
}

func (ch AddressChannel) Receive() Address {
	return <-ch
}

//Date
func (ch DateChannel) Send(date Date) {
	ch <- date
}

func (ch DateChannel) Receive() Date {
	return <-ch
}

// Bye
func (ch ByeChannel) Send(bye Bye) {
	ch <- bye
}

func (ch ByeChannel) Receive() Bye {
	return <-ch
}

// SessionChannel methods implementation

// Sends

func (schan *SessionChannel) sendLabel(label Label) {
	schan.labelChannel.Send(label)
}

func (schan *SessionChannel) sendAddress(address Address) {
	//schan.addressChannel <- address
	schan.addressChannel.Send(address)
}

func (schan *SessionChannel) sendDate(date Date) {
	//schan.dateChannel <- date
	schan.dateChannel.Send(date)
}

func (schan *SessionChannel) sendBye(bye Bye) {
	schan.byeChannel.Send(bye)
}

// Receives

func (schan *SessionChannel) receiveLabel() Label {
	return schan.labelChannel.Receive()
}

func (schan *SessionChannel) receiveAddress() Address {
	//return <-schan.addressChannel
	return schan.addressChannel.Receive()
}

func (schan *SessionChannel) receiveDate() Date {
	return schan.dateChannel.Receive()
}

func (schan *SessionChannel) receiveBye() Bye {
	return schan.byeChannel.Receive()
}

func Sender(schan *SessionChannel, endChannel chan bool) {
	funds, quote, buyer1quote := 30, 50, 25
	if quote-buyer1quote <= funds {
		schan.sendLabel("checkout")
		schan.sendAddress("Main Street")
		date := schan.receiveDate()
		println("date is ", date)
		endChannel <- true
	} else {
		schan.sendLabel("quit")
		schan.sendBye(Bye{})
		endChannel <- true
	}
}

func Receiver(schan *SessionChannel, endChannel chan bool) {
	label := schan.receiveLabel()
	if label == "checkout" {
		address := schan.receiveAddress()
		println("address is ", address)
		schan.sendDate("2018-01-20")
		endChannel <- true
	} else if label == "quit" {
		schan.receiveBye()
		println("Received bye message ")
		endChannel <- true
	} else {
		panic("Unexpected choice")
	}
}

func main() {
	schan := new(SessionChannel)
	schan.make()
	endChannel := make(chan bool)
	go Sender(schan, endChannel)
	go Receiver(schan, endChannel)
	// Wait for goroutines to finish
	<-endChannel
	<-endChannel
}
