package main

var count = 4


func Sender(ch chan int, endChannel chan bool) {
	println("count = ",count)
	ch <- 42
	if count > 0  {
		count--
		Sender(ch,endChannel)
	} else {
		endChannel <- true
	}
}


func Receiver(ch chan int, endChannel chan bool) {
	val := <-ch
	println("received ", val)
	endChannel <- true
}






func main() {
	ch := make(chan int)
	endChannel := make(chan bool)
	go Sender(ch,endChannel)
	go Receiver(ch,endChannel)
	<-endChannel
	<-endChannel
}
