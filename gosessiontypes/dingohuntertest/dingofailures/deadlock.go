package main

type end struct{}

// !int . ?string . end
func party1(intChannel chan int, stringChannel chan string, endChannel chan end) {
	//!int
	intChannel <- 42
	// ?string
	<-stringChannel
	// end
	endChannel <- end{}

}

// !string . ?int . end
func party2(intChannel chan int, stringChannel chan string, endChannel chan end) {
	//!string
	stringChannel <- "Hello World"
	// ?int
	<-intChannel
	//end
	endChannel <- end{}
}

func main() {
	intChannel := make(chan int)
	stringChannel := make(chan string)
	endChannel := make(chan end)
	go party1(intChannel, stringChannel, endChannel)
	go party2(intChannel, stringChannel, endChannel)
	<-endChannel
	<-endChannel
}
