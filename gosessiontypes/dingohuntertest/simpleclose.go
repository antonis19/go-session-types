package main

func Sender(ch chan <- int) {
	ch <-42	
	close(ch)  // <------
}

func Receiver(ch <- chan int) {
	<-ch
}

func main() {
	ch := make(chan int)
	go Sender(ch)
	Receiver(ch)
}
