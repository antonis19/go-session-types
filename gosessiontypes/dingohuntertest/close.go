package main

func Sender(ch chan <- int, end chan <- bool) {
	ch <-42	
	close(ch)  // <------
	end <- true
}

func Receiver(ch <- chan int, end chan <- bool) {
	<-ch
	end <-true
}

func main() {
	ch := make(chan int)
	end := make(chan bool)
	go Sender(ch,end)
	go Receiver(ch,end)
	<-end
	<-end
}
