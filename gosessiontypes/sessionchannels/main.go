package main


type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
}

func makeSessionChannel() SessionChannel {
	return SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
	}
}


func producer(sessionChannel SessionChannel) {
	sessionChannel.boolChannel <- true
	sessionChannel.stringChannel <- "Hello World"
	sessionChannel.intChannel <- 42
	//producer(sessionChannel)
}

func consumer(sessionChannel SessionChannel) {
	val1:=<-sessionChannel.boolChannel
	println("Received ",val1)
	
	val2:=<-sessionChannel.stringChannel
	println("Received ",val2)
	
	val3:=<-sessionChannel.intChannel
	println("Received ",val3)
	//consumer(sessionChannel)
}

func main() {
	sessionChannel := makeSessionChannel()
	go producer(sessionChannel)
	consumer(sessionChannel)
	println("Exiting")
}
