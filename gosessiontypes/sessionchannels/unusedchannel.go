package main


type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
	floatChannel  chan float64
}

func makeSessionChannel() SessionChannel {
	return SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		floatChannel : make(chan float64),
	}
}


func producer(sessionChannel SessionChannel) {
	sessionChannel.boolChannel <- true
	sessionChannel.stringChannel <- "Hello World"
	sessionChannel.intChannel <- 42
	//producer(sessionChannel)
}

func consumer(sessionChannel SessionChannel) {
	val1:=<-sessionChannel.boolChannel
	println("Received ",val1)
	
	val2:=<-sessionChannel.stringChannel
	println("Received ",val2)
	
	val3:=<-sessionChannel.intChannel
	println("Received ",val3)
	//consumer(sessionChannel)
}

func main() {
	sessionChannel := SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		floatChannel : make(chan float64),
	}
	go producer(sessionChannel)
	consumer(sessionChannel)
	println("Exiting")
}
