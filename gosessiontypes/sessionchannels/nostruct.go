package main


func producer(boolChannel chan bool, stringChannel chan string, intChannel chan int) {
	boolChannel <- true
	stringChannel <- "Hello World"
	intChannel <- 42
	//producer(sessionChannel)
}

func consumer(boolChannel chan bool, stringChannel chan string, intChannel chan int) {
	val1:=<-boolChannel
	println("Received ",val1)
	
	val2:=<-stringChannel
	println("Received ",val2)
	
	val3:=<-intChannel
	println("Received ",val3)
	//consumer(sessionChannel)
}

func main() {
	boolChannel := make(chan bool)
	stringChannel := make(chan string)
	intChannel := make(chan int)
	go producer(boolChannel, stringChannel, intChannel)
	consumer(boolChannel, stringChannel, intChannel)
	println("Exiting")
}
