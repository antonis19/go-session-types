package main


type Address struct {
	addressField string
}

type Date struct {
	dateField string
}

type Bye struct{}

type SessionChannel struct {
	boolChannel   chan bool
	stringChannel chan string
	intChannel    chan int
	addressChannel chan Address
	dateChannel chan Date
	byeChannel chan Bye
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel {
		boolChannel : make(chan bool),
		stringChannel : make(chan string),
		intChannel : make(chan int),
		addressChannel : make (chan Address),
		dateChannel : make (chan Date),
		byeChannel : make(chan Bye),
	}
}


func buyer1(sChanBuyer2 *SessionChannel, sChanSeller *SessionChannel, end chan <- bool) {
	title := "The Go Programming Language"
	// Buyer1 -> Seller string
	sChanSeller.stringChannel <- title
	// Seller -> Buyer1 int
	quote:= <-sChanSeller.intChannel
	println("[Buyer1] quote is ",quote)
	// Buyer1 -> Buyer2 string
	sChanBuyer2.intChannel <- quote/2
	// end
	end <- true
}


func buyer2(sChanBuyer1 *SessionChannel, sChanSeller *SessionChannel, end chan <- bool) {
	funds := 30
	// Seller -> Buyer2 int
	quote := <-sChanSeller.intChannel
	println("[Buyer2] quote is ",quote)
	// Buyer1 -> Buyer2 int
	buyer1Quote := <-sChanBuyer1.intChannel
	println("[Buyer2] buyer1Quote is", buyer1Quote)
	// internal choice
	if quote - buyer1Quote <= funds {
		// Buyer2 -> Seller Address
		sChanSeller.addressChannel <- Address { addressField: "Jane Street",}
		// Seller -> Buyer2 Date
		date := <-sChanSeller.dateChannel
		println("[Buyer2] date is ", date.dateField)
	} else {
		// Buyer2 -> Seller Bye
		sChanSeller.byeChannel <- Bye{}
	}
	end <- true
		
}

func seller(sChanBuyer1 *SessionChannel, sChanBuyer2 *SessionChannel, end chan <- bool) {
	// Buyer1 -> Seller string
	title := <-sChanBuyer1.stringChannel
	println("[Seller] title is ",title)
	quote:= 50
	// Seller -> Buyer 1 quote
	sChanBuyer1.intChannel <- quote
	// Seller -> Buyer 2 quote
	sChanBuyer2.intChannel <- quote
	// external choice
	select {
		// Buyer2 -> Seller Address
		case address:= <-sChanBuyer2.addressChannel :
			println("[Seller] address is : ",address.addressField)
			date:= Date{ dateField : "2018-01-20"}
			// Seller -> Buyer2 Date
			sChanBuyer2.dateChannel <- date
		// Buyer2 -> Seller Bye
		case  <-sChanBuyer2.byeChannel :
			println("[Seller] got the bye message")
	} 		
	end <- true
}

func main() {
	const nParties = 3
	sChanBuyer1Buyer2, sChanBuyer1Seller, sChanBuyer2Seller := makeSessionChannel(), makeSessionChannel(), makeSessionChannel()
	end :=  make(chan bool)
	go buyer1(sChanBuyer1Buyer2,sChanBuyer1Seller,end)
	go buyer2(sChanBuyer1Buyer2,sChanBuyer2Seller,end)
	go seller(sChanBuyer1Seller,sChanBuyer2Seller,end)
	// wait for parties to finish
	<-end
	<-end
	<-end
	println("Exiting")
}
