package main

import (
	"sync"
)

type Address string
type Date string
type Bye struct{}

type SessionChannel struct {
	stringChannel  chan string
	intChannel     chan int
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel{
		stringChannel:  make(chan string),
		intChannel:     make(chan int),
		addressChannel: make(chan Address),
		dateChannel:    make(chan Date),
		byeChannel:     make(chan Bye),
	}
}

func (schan *SessionChannel) make() {
	schan.stringChannel = make(chan string)
	schan.intChannel = make(chan int)
	schan.addressChannel = make(chan Address)
	schan.dateChannel = make(chan Date)
	schan.byeChannel = make(chan Bye)
}

func buyer1(sChanBuyer2 *SessionChannel, sChanSeller *SessionChannel, waitgroup *sync.WaitGroup) {
	title := "The Go Programming Language"
	// Buyer1 -> Seller string
	sChanSeller.stringChannel <- title
	// Seller -> Buyer1 int
	quote := <-sChanSeller.intChannel
	println("[Buyer1] quote is ", quote)
	// Buyer1 -> Buyer2 string
	sChanBuyer2.intChannel <- quote / 2
	// endChannel
	waitgroup.Done()
}

func buyer2(sChanBuyer1 *SessionChannel, sChanSeller *SessionChannel, waitgroup *sync.WaitGroup) {
	funds := 30
	// Seller -> Buyer2 int
	quote := <-sChanSeller.intChannel
	println("[Buyer2] quote is ", quote)
	// Buyer1 -> Buyer2 int
	buyer1Quote := <-sChanBuyer1.intChannel
	println("[Buyer2] buyer1Quote is", buyer1Quote)
	// internal choice
	if quote-buyer1Quote <= funds {
		// Buyer2 -> Seller Address
		sChanSeller.addressChannel <- Address("Jane Street")
		// Seller -> Buyer2 Date
		date := <-sChanSeller.dateChannel
		println("[Buyer2] date is ", date)
	} else {
		// Buyer2 -> Seller Bye
		sChanSeller.byeChannel <- Bye{}
	}
	waitgroup.Done()
}

func seller(sChanBuyer1 *SessionChannel, sChanBuyer2 *SessionChannel, waitgroup *sync.WaitGroup) {
	// Buyer1 -> Seller string
	title := <-sChanBuyer1.stringChannel
	println("[Seller] title is ", title)
	quote := 50
	// Seller -> Buyer 1 quote
	sChanBuyer1.intChannel <- quote
	// Seller -> Buyer 2 quote
	sChanBuyer2.intChannel <- quote
	// external choice
	select {
	// Buyer2 -> Seller Address
	case address := <-sChanBuyer2.addressChannel:
		println("[Seller] address is : ", address)
		date := Date("2018-01-20")
		// Seller -> Buyer2 Date
		sChanBuyer2.dateChannel <- date
	// Buyer2 -> Seller Bye
	case <-sChanBuyer2.byeChannel:
		println("[Seller] got the bye message")
	}
	waitgroup.Done()
}

func main() {
	var waitgroup sync.WaitGroup
	sChanBuyer1Buyer2, sChanBuyer1Seller, sChanBuyer2Seller := new(SessionChannel), new(SessionChannel), new(SessionChannel)
	sChanBuyer1Buyer2.make()
	sChanBuyer1Seller.make()
	sChanBuyer2Seller.make()
	waitgroup.Add(3)
	go buyer1(sChanBuyer1Buyer2, sChanBuyer1Seller, &waitgroup)
	go buyer2(sChanBuyer1Buyer2, sChanBuyer2Seller, &waitgroup)
	go seller(sChanBuyer1Seller, sChanBuyer2Seller, &waitgroup)
	// wait for parties to finish
	waitgroup.Wait()
	println("Exiting")
}
