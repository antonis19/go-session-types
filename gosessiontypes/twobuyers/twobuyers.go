package main

import "fmt"

type Address string
type Date string
type Bye struct{}

type end struct{}

type SessionChannel struct {
	stringChannel  chan string
	intChannel     chan int
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

func makeSessionChannel() *SessionChannel {
	return &SessionChannel{
		stringChannel:  make(chan string),
		intChannel:     make(chan int),
		addressChannel: make(chan Address),
		dateChannel:    make(chan Date),
		byeChannel:     make(chan Bye),
	}
}

func (schan *SessionChannel) make() {
	schan.stringChannel = make(chan string)
	schan.intChannel = make(chan int)
	schan.addressChannel = make(chan Address)
	schan.dateChannel = make(chan Date)
	schan.byeChannel = make(chan Bye)
}

func buyer1(sChanBuyer2 *SessionChannel, sChanSeller *SessionChannel, endChannel chan<- end) {
	title := "The Go Programming Language"
	// Buyer1 -> Seller string
	sChanSeller.stringChannel <- title
	// Seller -> Buyer1 int
	quote := <-sChanSeller.intChannel
	fmt.Println("[Buyer1] quote is ", quote)
	// Buyer1 -> Buyer2 string
	sChanBuyer2.intChannel <- quote / 2
	// endChannel
	endChannel <- end{}
}

func buyer2(sChanBuyer1 *SessionChannel, sChanSeller *SessionChannel, endChannel chan<- end) {
	funds := 30
	// Seller -> Buyer2 int
	quote := <-sChanSeller.intChannel
	fmt.Println("[Buyer2] quote is ", quote)
	// Buyer1 -> Buyer2 int
	buyer1Quote := <-sChanBuyer1.intChannel
	fmt.Println("[Buyer2] buyer1Quote is", buyer1Quote)
	// internal choice
	if quote-buyer1Quote <= funds {
		// Buyer2 -> Seller Address
		sChanSeller.addressChannel <- Address("Jane Street")
		// Seller -> Buyer2 Date
		date := <-sChanSeller.dateChannel
		fmt.Println("[Buyer2] date is ", date)
		endChannel <- end{}
	} else {
		// Buyer2 -> Seller Bye
		sChanSeller.byeChannel <- Bye{}
		endChannel <- end{}
	}
}

func seller(sChanBuyer1 *SessionChannel, sChanBuyer2 *SessionChannel, endChannel chan<- end) {
	// Buyer1 -> Seller string
	title := <-sChanBuyer1.stringChannel
	fmt.Println("[Seller] title is ", title)
	quote := 50
	// Seller -> Buyer 1 quote
	sChanBuyer1.intChannel <- quote
	// Seller -> Buyer 2 quote
	sChanBuyer2.intChannel <- quote
	// external choice
	select {
	// Buyer2 -> Seller Address
	case address := <-sChanBuyer2.addressChannel:
		fmt.Println("[Seller] address is : ", address)
		date := Date("2018-01-20")
		// Seller -> Buyer2 Date
		sChanBuyer2.dateChannel <- date
		endChannel <- end{}
	// Buyer2 -> Seller Bye
	case <-sChanBuyer2.byeChannel:
		fmt.Println("[Seller] got the bye message")
		endChannel <- end{}
	}
}

func main() {
	sChanBuyer1Buyer2, sChanBuyer1Seller, sChanBuyer2Seller := new(SessionChannel), new(SessionChannel), new(SessionChannel)
	sChanBuyer1Buyer2.make()
	sChanBuyer1Seller.make()
	sChanBuyer2Seller.make()
	endChannel := make(chan end)
	go buyer1(sChanBuyer1Buyer2, sChanBuyer1Seller, endChannel)
	go buyer2(sChanBuyer1Buyer2, sChanBuyer2Seller, endChannel)
	go seller(sChanBuyer1Seller, sChanBuyer2Seller, endChannel)
	// wait for parties to finish
	<-endChannel
	<-endChannel
	<-endChannel
	fmt.Println("Exiting")
}
