package main

import "fmt"

// The data types that will be exchanged
type Address string
type Date string
type Bye struct{}

type Label struct{}

// The two labels used in the protocol
type OK Label
type QUIT Label

type end struct{}

// Channels is a struct encapsulating the channels for datatypes that will be exchanged
type Channels struct {
	stringChannel  chan string
	intChannel     chan int
	addressChannel chan Address
	dateChannel    chan Date
	byeChannel     chan Bye
}

// SessionChannel
type SessionChannel struct {
	okChannel   chan OK
	quitChannel chan QUIT
	in          *Channels
	out         *Channels
}

func (channels *Channels) make(bufsize int) {
	channels.stringChannel = make(chan string, bufsize)
	channels.intChannel = make(chan int, bufsize)
	channels.addressChannel = make(chan Address, bufsize)
	channels.dateChannel = make(chan Date, bufsize)
	channels.byeChannel = make(chan Bye, bufsize)
}

func (schan *SessionChannel) make(bufsize int) {
	schan.in = new(Channels)
	schan.out = new(Channels)
	schan.in.make(bufsize)
	schan.out.make(bufsize)
	// make label channels
	schan.okChannel = make(chan OK)
	schan.quitChannel = make(chan QUIT)
}

// This function returns a pair of SessionChannels that share the same
// underlying channels, just with different names
func newSessionChannelPair(bufsize int) (*SessionChannel, *SessionChannel) {
	schan1 := new(SessionChannel)
	schan1.make(bufsize)
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	// schan1 and schan2 share the same channels, just with different names
	schan2.in = schan1.out
	schan2.out = schan1.in
	// these channels are for sending/receiving labels
	schan2.okChannel = schan1.okChannel
	schan2.quitChannel = schan1.quitChannel
	return schan1, schan2
}

// An alternative to calling the function newSessionChannelPair
// is to do schan2 := schan1.dual()
func (schan1 *SessionChannel) dual() *SessionChannel {
	schan2 := new(SessionChannel)
	schan2.in = new(Channels)
	schan2.out = new(Channels)
	schan2.in = schan1.out
	schan2.out = schan1.in
	schan2.okChannel = schan1.okChannel
	schan2.quitChannel = schan1.quitChannel
	return schan2
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetInputChannels() []interface{} {
	return []interface{}{schan.in.stringChannel, schan.in.intChannel,
		schan.in.addressChannel, schan.in.dateChannel, schan.in.byeChannel}
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetOutputChannels() []interface{} {
	return []interface{}{schan.out.stringChannel, schan.out.intChannel,
		schan.out.addressChannel, schan.out.dateChannel, schan.out.byeChannel}
}

// This is supposed to be used by distchan
func (schan *SessionChannel) GetLabelChannels() []interface{} {
	return []interface{}{schan.okChannel, schan.quitChannel}
}

// Send/Receive operations on datatypes hide implementation details
// We cannot do the same thing for labels because select-case requires explicit
// channel operations on the cases

// Sends

func (schan *SessionChannel) sendString(str string) {
	schan.out.stringChannel <- str
}

func (schan *SessionChannel) sendInt(n int) {
	schan.out.intChannel <- n
}

func (schan *SessionChannel) sendAddress(address Address) {
	schan.out.addressChannel <- address
}

func (schan *SessionChannel) sendDate(date Date) {
	schan.out.dateChannel <- date
}

func (schan *SessionChannel) sendBye(bye Bye) {
	schan.out.byeChannel <- bye
}

//Receives

func (schan *SessionChannel) receiveString() string {
	return <-schan.in.stringChannel
}

func (schan *SessionChannel) receiveInt() int {
	return <-schan.in.intChannel
}

func (schan *SessionChannel) receiveAddress() Address {
	return <-schan.in.addressChannel
}

func (schan *SessionChannel) receiveDate() Date {
	return <-schan.in.dateChannel
}

func (schan *SessionChannel) receiveBye() Bye {
	return <-schan.in.byeChannel
}

func buyer1(sChanBuyer2 *SessionChannel, sChanSeller *SessionChannel, endChannel chan<- end) {
	title := "The Go Programming Language"
	// Buyer1 -> Seller string
	sChanSeller.sendString(title)
	// Seller -> Buyer1 int
	quote := sChanSeller.receiveInt()
	fmt.Println("[Buyer1] quote is ", quote)
	// Buyer1 -> Buyer2 string
	sChanBuyer2.sendInt(quote / 2)
	// endChannel
	endChannel <- end{}
}

func buyer2(sChanBuyer1 *SessionChannel, sChanSeller *SessionChannel, endChannel chan<- end) {
	funds := 30
	// Seller -> Buyer2 int
	quote := sChanSeller.receiveInt()
	fmt.Println("[Buyer2] quote is ", quote)
	// Buyer1 -> Buyer2 int
	buyer1Quote := sChanBuyer1.receiveInt()
	fmt.Println("[Buyer2] buyer1Quote is", buyer1Quote)
	// internal choice
	if quote-buyer1Quote <= funds {
		sChanSeller.okChannel <- OK{}
		// Buyer2 -> Seller Address
		sChanSeller.sendAddress(Address("Jane Street"))
		// Seller -> Buyer2 Date
		date := sChanSeller.receiveDate()
		fmt.Println("[Buyer2] date is ", date)
		endChannel <- end{}
	} else {
		sChanSeller.quitChannel <- QUIT{}
		// Buyer2 -> Seller Bye
		sChanSeller.sendBye(Bye{})
		endChannel <- end{}
	}
}

func seller(sChanBuyer1 *SessionChannel, sChanBuyer2 *SessionChannel, endChannel chan<- end) {
	// Buyer1 -> Seller string
	title := sChanBuyer1.receiveString()
	fmt.Println("[Seller] title is ", title)
	quote := 50
	// Seller -> Buyer 1 quote
	sChanBuyer1.sendInt(quote)
	// Seller -> Buyer 2 quote
	sChanBuyer2.sendInt(quote)
	// external choice
	select {
	case <-sChanBuyer2.okChannel:
		// Buyer2 -> Seller Address
		address := sChanBuyer2.receiveAddress()
		fmt.Println("[Seller] address is : ", address)
		date := Date("2018-01-20")
		// Seller -> Buyer2 Date
		sChanBuyer2.sendDate(date)
		endChannel <- end{}
	// Buyer2 -> Seller Bye
	case <-sChanBuyer2.quitChannel:
		sChanBuyer2.receiveBye()
		fmt.Println("[Seller] got the bye message")
		endChannel <- end{}
	}
}

func main() {
	bufsize := 4 // choose an  arbitary buffer size
	sChanBuyer1Buyer2_1, sChanBuyer1Buyer2_2 := newSessionChannelPair(bufsize)
	sChanBuyer1Seller_1, sChanBuyer1Seller_2 := newSessionChannelPair(bufsize)
	sChanBuyer2Seller_1, sChanBuyer2Seller_2 := newSessionChannelPair(bufsize)
	endChannel := make(chan end)
	go buyer1(sChanBuyer1Buyer2_1, sChanBuyer1Seller_1, endChannel)
	go buyer2(sChanBuyer1Buyer2_2, sChanBuyer2Seller_1, endChannel)
	go seller(sChanBuyer1Seller_2, sChanBuyer2Seller_2, endChannel)
	// Wait for goroutines to finish
	<-endChannel
	<-endChannel
	<-endChannel
}
