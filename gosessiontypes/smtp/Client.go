package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"

	. "./schan"
	"bitbucket.org/antonis19/distchan"
)

type end struct{}

var SERVER_HOST = `localhost`
var SERVER_PORT = 3333

func connect(host string, port int) net.Conn {
	conn, err := net.Dial("tcp", host+":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalln(err)
	}
	return conn
}

func SMTPClient(schanServer *SessionChannel, endChannel chan end) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("[Client] Enter Command :")
	scanner.Scan()
	command := scanner.Text()
	if command == "EHLO" {
		schanServer.Out.EHLOChannel <- EHLO{}
		fmt.Println("[Client] Enter Domain :")
		scanner.Scan()
		domain := Domain(scanner.Text())
		schanServer.SendDomain(domain)
		fmt.Println("[Client] Enter FromAddress: ")
		scanner.Scan()
		fromAddress := FromAddress(scanner.Text())
		schanServer.SendFromAddress(fromAddress)
		fmt.Println("[Client] Enter ToAddress: ")
		scanner.Scan()
		toAddress := ToAddress(scanner.Text())
		schanServer.SendToAddress(toAddress)
		fmt.Println("[Client] Enter Message: ")
		scanner.Scan()
		message := Message(scanner.Text())
		schanServer.SendMessage(message)
		SMTPClient(schanServer, endChannel)
	} else if command == "HELP" {
		schanServer.Out.HELPChannel <- HELP{}
		helpMessage := schanServer.ReceiveHelpMessage()
		fmt.Println("[Client] Received Help Message : ", helpMessage)
		SMTPClient(schanServer, endChannel)
	} else { // QUIT
		schanServer.Out.QUITChannel <- QUIT{}
		endChannel <- end{}
	}
}

func main() {
	bufsize := 4 // pick an arbitary buffer size
	schan, _ := NewSessionChannelPair(bufsize)
	endChannel := make(chan end)
	conn := connect(SERVER_HOST, SERVER_PORT)
	client, err := distchan.NewClient(conn, schan)
	if err != nil {
		log.Fatalln("Could not create new client")
	}
	client.Start()
	go SMTPClient(schan, endChannel)
	<-endChannel
	client.Done()
}
