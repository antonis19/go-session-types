package main

import (
	"bufio"
	"fmt"
	"os"

	. "./schan"
)

type end struct{}

func SMTPClient(schanServer *SessionChannel, endChannel chan end) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("[Client] Enter Command :")
	scanner.Scan()
	command := scanner.Text()
	if command == "EHLO" {
		schanServer.Out.EHLOChannel <- EHLO{}
		fmt.Println("[Client] Enter Domain :")
		scanner.Scan()
		domain := Domain(scanner.Text())
		schanServer.SendDomain(domain)
		fmt.Println("[Client] Enter FromAddress: ")
		scanner.Scan()
		fromAddress := FromAddress(scanner.Text())
		schanServer.SendFromAddress(fromAddress)
		fmt.Println("[Client] Enter ToAddress: ")
		scanner.Scan()
		toAddress := ToAddress(scanner.Text())
		schanServer.SendToAddress(toAddress)
		fmt.Println("[Client] Enter Message: ")
		scanner.Scan()
		message := Message(scanner.Text())
		schanServer.SendMessage(message)
		SMTPClient(schanServer, endChannel)
	} else if command == "HELP" {
		schanServer.Out.HELPChannel <- HELP{}
		helpMessage := schanServer.ReceiveHelpMessage()
		fmt.Println("[Client] Received Help Message : ", helpMessage)
		SMTPClient(schanServer, endChannel)
	} else { // QUIT
		schanServer.Out.QUITChannel <- QUIT{}
		endChannel <- end{}
	}
}

func SMTPServer(schanClient *SessionChannel, endChannel chan end) {
	var helpMessage HelpMessage = `Available commands : EHLO, HELP, SIZE, QUIT`
	var size int = 10000 // maximum supported message length
	select {
	case <-schanClient.In.EHLOChannel:
		fmt.Println("[Server] Received EHLO command")
		domain := schanClient.ReceiveDomain()
		fmt.Println("[Server] Received domain : ", domain)
		fromAddress := schanClient.ReceiveFromAddress()
		fmt.Println("[Server] Received fromAddress : ", fromAddress)
		toAddress := schanClient.ReceiveToAddress()
		fmt.Println("[Server] Received toAddress : ", toAddress)
		message := schanClient.ReceiveMessage()
		fmt.Println("[Server] Received message : ", message)
		SMTPServer(schanClient, endChannel)
	case <-schanClient.In.HELPChannel:
		fmt.Println("[Server] Received HELP command")
		schanClient.SendHelpMessage(helpMessage)
		SMTPServer(schanClient, endChannel)
	case <-schanClient.In.SIZEChannel:
		fmt.Println("[Server] Received SIZE command")
		schanClient.SendInt(size)
		SMTPServer(schanClient, endChannel)
	case <-schanClient.In.QUITChannel:
		fmt.Println("[Server] Received QUIT command")
		endChannel <- end{}
	}
}

func main() {
	bufsize := 4 // pick an arbitrary buffer size
	schan1, schan2 := NewSessionChannelPair(bufsize)
	endChannel := make(chan end)
	go SMTPClient(schan1, endChannel)
	go SMTPServer(schan2, endChannel)
	<-endChannel
	<-endChannel
}
