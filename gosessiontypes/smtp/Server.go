package main

import (
	"fmt"
	"log"
	"net"
	"strconv"

	. "./schan"
	"bitbucket.org/antonis19/distchan"
)

type end struct{}

var SERVER_HOST = `localhost`
var SERVER_PORT = 3333

func createListener(host string, port int) net.Listener {
	ln, err := net.Listen("tcp", host+":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalln(err)
	}
	return ln
}

func SMTPServer(schanClient *SessionChannel, endChannel chan end) {
	var helpMessage HelpMessage = `Available commands : EHLO, HELP, SIZE, QUIT`
	var size int = 10000 // maximum supported message length
	select {
	case <-schanClient.In.EHLOChannel:
		fmt.Println("[Server] Received EHLO command")
		domain := schanClient.ReceiveDomain()
		fmt.Println("[Server] Received domain : ", domain)
		fromAddress := schanClient.ReceiveFromAddress()
		fmt.Println("[Server] Received fromAddress : ", fromAddress)
		toAddress := schanClient.ReceiveToAddress()
		fmt.Println("[Server] Received toAddress : ", toAddress)
		message := schanClient.ReceiveMessage()
		fmt.Println("[Server] Received message : ", message)
		SMTPServer(schanClient, endChannel)
	case <-schanClient.In.HELPChannel:
		fmt.Println("[Server] Received HELP command")
		schanClient.SendHelpMessage(helpMessage)
		SMTPServer(schanClient, endChannel)
	case <-schanClient.In.SIZEChannel:
		fmt.Println("[Server] Received SIZE command")
		schanClient.SendInt(size)
		SMTPServer(schanClient, endChannel)
	case <-schanClient.In.QUITChannel:
		fmt.Println("[Server] Received QUIT command")
		endChannel <- end{}
	}
}

func main() {
	bufsize := 4 // pick an arbitary buffer size
	schan, _ := NewSessionChannelPair(bufsize)
	endChannel := make(chan end)
	ln := createListener(SERVER_HOST, SERVER_PORT)
	server, err := distchan.NewServer(ln, schan)
	if err != nil {
		log.Fatalln("Could not create new server")
	}
	server.Start()
	server.WaitUntilReady()
	go SMTPServer(schan, endChannel)
	<-endChannel
	server.Done()
}
